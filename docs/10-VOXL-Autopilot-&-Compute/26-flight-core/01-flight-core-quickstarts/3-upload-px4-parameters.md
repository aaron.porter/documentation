---
layout: default
title: Upload PX4 Parameters
parent: Flight Core Quickstarts
nav_order: 3
permalink: /upload-px4-parameters/
---

# Upload PX4 Parameters

## 1. Connect to PX4 via USB and Open QGroundControl

After flashing the PX4 firmware, leave the USB cable connected and open QGroundControl. It should connect and show red icons for airframe, sensors, radio, flight modes, and power. We will go through and set up these sections one by one!

![1-everything_wiped_to_defaults.png](/images/voxl-sdk/qgc/1-everything_wiped_to_defaults.png)

## 2. Wipe parameters back to default

If the 5 items listed in the last step are not all red, or you just want to be thorough, reset all px4 parameters back to default with the reset button pictured below. If the "tools" button does not show up, you will first need to enter the parameters menu. Note, this will wipe any and all calibration data if you have calibrated this board before!!

![2-reset-to-defaults.png](/images/voxl-sdk/qgc/2-reset-to-defaults.png)

## 3. Download PX4 Parameters File

We suggest starting with our default set of PX4 parameters. Since we build from the PX4 firmware master branch, new parameters are added very frequently and we will update the parameters in this repository accordingly.

[px4-parameters](https://gitlab.com/voxl-public/flight-core-px4/px4-parameters)

Currently this project is tested on a ModalAI M500 platform with a VOXL and Flight Core flight controller. The flight controller orientation is taken into consideration in this parameter file.

## 4. Upload Parameters file

The parameter files available from the above repository are trimmed of any sensor or radio calibration parameters so they can be uploaded via QGroundControl without recalibrating. You can use the trim_cal_params.sh script to trim your own parameter files if you have customizations that you wish to share between airframes without overwriting calibration. This trimming also means you can come back to this step and upload new parameter files without having to repeat the following calibration steps.

The presence of many PX4 parameters are contingent upon other parameters. As a result it's necessary to upload parameter files TWICE. That process looks like this:

Upload parameters once:

![4.1-upload-parameters.png](/images/voxl-sdk/qgc/4.1-upload-parameters.png)

Many warnings will likely appear saying some parameters don't exist yet. We know, don't worry!! Close the warnings and reboot PX4.

![4.2-warning-about-missing-params.png](/images/voxl-sdk/qgc/4.2-warning-about-missing-params.png)

Now load the same params file a second time.

![4.3-load-from-file-a-second-time.png](/images/voxl-sdk/qgc/4.3-load-from-file-a-second-time.png)

All of the parameters should have taken this time through. A quick sanity check is to look for the PWM_MIN parameter and check that its value is set to 1075.

![4.4-check-pwm-min.png](/images/voxl-sdk/qgc/4.4-check-pwm-min.png)

## Parameter Reference

As you continue to work with PX4 on the Flight Core, you will want to refer to the official [PX4 parameters reference](https://docs.px4.io/master/en/advanced_config/parameter_reference.html). We build PX4 firmware from master branch so make sure you are using the master-branch of the documentation page.

[Next: Calibrate Sensors](/px4-calibrate-sensors/){: .btn .btn-green }
