---
layout: default
title: Update Flight Core Firmware
parent: Flight Core Quickstarts
nav_order: 2
permalink: /update-flight-core-firmware/
---

# Update Flight Core and VOXL Flight Firmware
{: .no_toc }

*ModalAI Top Tip:* The Flight Core  and VOXL Flight ships with the latest firmware available at the time of the shipment.  The following may be unnecessary if you've checked the firmware version through QGroundControl and it meets your needs.  If this is the case, then you can skip to the [Upload PX4 Parameters](/upload-px4-parameters/) section.

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Summary

VOXL Flight and Flight Core run PX4, which can be field updated over USB.

## Hardware Requirements

- [Flight Core](/flight-core-datasheet/) or [VOXL Flight](/voxl-flight-datasheet/)
- [Power Module](/power-module-datasheets/) and [MCBL-00003](/cable-datasheets/#mcbl-00003/) or [MCBL-00001](/cable-datasheets/#mcbl-00001/)
- USB-to-JST cable [MCBL-00010](/cable-datasheets/#mcbl-00010/))
- Host computer with available USB port
- QGroundControl 4.0+

## Update Procedure

Firmware update of the Flight Core or VOXL Flight is supported starting in QGC v4.0.

### Update Using QGroundControl - Mainline Builds

Applicable if using PX4 v1.11+

- Obtain MCBL-00010](/cable-datasheets/#mcbl-00010/) (JST to microUSB female) + microUSB cable
- Connect calbe to Flight Core `J3` or VOXL Flight `J1006` (the blue connector) to Host PC  with QGC 4.0+
- In QGC, go to the Firmware tab
- **Power cycle** the Flight Core or VOXL Flight (the bootloader runs and the green LED flashes quickly)
  - NOTE: you must power cycle with USB attached in order for the bootloader to activate
- When the `Firmware Setup` screen shows, select the `PX4 Flight Stack v1.11 - Stable Release` option
- Click `OK`
- During the FW update,  the LEDs will flash red/green
- The board will reboot at the end of the process

### Update Using QGroundControl - ModalAI Build

Applicable if using PX4 v1.10.

- Download the ModalAI release `.px4` file from the [Flight Core Firmware page](/flight-core-firmware/)
- Obtain [MCBL-00010](/cable-datasheets/#mcbl-00010/) (JST to microUSB female) + microUSB cable
- Connect cable to Flight Core `J3` or VOXL Flight `J1006` (the blue connector) to Host PC  with QGC 4.0+
- In QGC, go to the Firmware tab
- **Power cycle** the Flight Core or VOXL Flight (the bootloader runs and the green LED flashes quickly)
  - NOTE: you must power cycle with USB attached in order for the bootloader to activate
- When the `Firmware Setup` screen shows, click on the `Advanced settings` button
- Select `Custom firmware file`
- Click `OK`
- Select the `.px4` file you downloaded in the first step
- During the FW update,  the LEDs will flash red/green
- The board will reboot at the end of the process

## Update Using USB and Utility Script

### Additional Software Requirements

- Install requirements:
  - python 2.7, pip, pyserial
- Clone the PX4 firmware: <https://github.com/PX4/Firmware>
- Locate the utility script at `Tools/upload.sh`

### Download Firmware File

- Download the ModalAI release `.px4` file from the [Flight Core Firmware page](/flight-core-firmware/)

### Perform Update

- Obtain [MCBL-00010](/cable-datasheets/#mcbl-00010/) (JST to microUSB female) + microUSB cable
- Connect calbe to Flight Core `J3` or VOXL Flight `J1006` (the blue connector) to Host PC
- Power on the Flight Core board
- Perform the FW update by running the following command, where the argument is the path to the `.px4` file

```bash
./Tools/upload.sh ./build/modalai_fc-v1_default/modalai_fc-v1_default.px4
```

If attemping to use the wrong firmware file, you will get a warning:

```bash
WARNING: Firmware not suitable for this board (board_type=41775 board_id=XXX)
```

[Next: Upload PX4 Parameters](/upload-px4-parameters/){: .btn .btn-green }
