---
layout: default
title: Flight Core Bootloader
parent: Flight Core User Guides
nav_order: 99
has_children: false
permalink: /flight-core-bootloader/
---

# Flight Core Bootloader
{: .no_toc }

## Changelog

| Release | Build Commit            | Description               | Link |
|--- |---                 |---                        | ---  |
| 1 | N/A | Alpha Release             | N/A  |
| 2 | [GitLab CI](https://gitlab.com/voxl-public/px4-bootloader-ci/commit/7bc2734a091650522789b9ab3c3b86d7a8a01022)  | QGC FW Update Support ([bug fix](https://github.com/PX4/Bootloader/commit/d83169d50c333ab33d8206a8a29f6efc9605c39b)) | [downloaded](https://storage.googleapis.com/modalai_public/flight_core/bootloader/v5.0-167-g7bc2734/modalai_fc_v1_bl.bin) |

## Bootloader Binary Location

A prebuilt binary of the latest bootloader can be [downloaded from here](https://storage.googleapis.com/flight-core-firmware-bootloader/latest/modalai_fc_v1_bl.bin)

## How to Build the Bootloader

You can build (and customize) the bootloader as needed.  Flight Core is supported in the PX4 Bootloader mainline repository.

```bash
git clone https://github.com/PX4/Bootloader
cd Bootloader
make modalai_fc_v1_bl
```

## In Field Bootloader Update Procedure

**ModalAI Top Tip!** You should know what you're doing before proceeding as this can brick the device.

- Download the bootloader binary
- Copy the file to the root of a MicroSD card
- Power off the Flight Core
- Insert the MicroSD card into the Flight Core's MicroSD card slot
- Power on the Flight Core
- Connect Flight Core to QGroundControl using the USB cable
  - Note: alternatively you can use the debug serial console
- If using USB, in QGC, go to the MAVLink Console terminal
- Verify the `modalai_fc_v1_bl.bin` is on the MicroSD card by running the following command:

```bash
ls /fs/microsd
```

The following should (roughly) display:

```bash
/fs/microsd:
 dataman
 log/
 modalai_fc_v1_bl.bin
```

- To update the bootloader, run the following command:

```bash
bl_update /fs/microsd/modalai_fc_v1_bl.bin
```

The following should display:

```bash
INFO  [bl_update] image validated, erasing bootloader...
ERROR [bl_update] erase error at 0x8000100
INFO  [bl_update] flashing...
INFO  [bl_update] verifying...
INFO  [bl_update] bootloader update complete
```

- Reboot the device and verify it boots up

```bash
reboot
```

## Flashing Using SEGGER JLink

You can use `JFlash` with a `.jlink` file with command like so:

```bash
loadbin modalai_fc_v1_bl.bin,0x08000000
```
