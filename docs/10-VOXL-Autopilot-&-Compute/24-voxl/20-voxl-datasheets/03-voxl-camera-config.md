---
layout: default
title: VOXL Camera Configs
parent: VOXL Datasheets
nav_order: 3
permalink: /voxl-camera-config/
---

# VOXL Camera Configurations
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Camera Ports

VOXL has 3 camera connectors Labeled J2, J3, J4 in the two images below.

![VOXL Core Connected](/images/voxl/voxl/voxl-core-bottom.png)

*Image 1 - VOXL Bottom View*

![VOXL Core USB](/images/voxl/voxl/voxl-core-top.png)

*Image 2 - VOXL Top View*

The connectors are attached to the following Camera Subsystem ports and have the following colloquial names. Note that the ports can be used in other configurations as described bellow, not just the purpose described by the port's colloquial name.

| Port  | Camera SS Port    | Colloquial Name               |
|------ |----------------   |------------------------------ |
| J2    | CSI0              | High-Resolution 4k RGB Port   |
| J3    | CSI1              | Stereo Camera Port            |
| J4    | CSI2              | Tracking Camera Port          |

## Configurations


### 1 Tracking + Stereo

| Port  | Camera                	| Camera ID |
|------ |---------------------------|-----------|
| J2    | Empty                 	| N/A       |
| J3    | Stereo Cameras        	| 1         |
| J4    | Tracking Camera       	| 0         |

### 2 Tracking Only

| Port  | Camera                	| Camera ID |
|------ |---------------------------|-----------|
| J2    | Empty                 	| N/A       |
| J3    | Empty                 	| N/A       |
| J4    | Tracking Camera       	| 0         |


### 3 High-Resolution + Stereo + Tracking

This is the default configuration for the M500 Drone

| Port  | Camera                	| Camera ID |
|------ |---------------------------|-----------|
| J2    | High-Resolution 4k RGB	| 0         |
| J3    | Stereo Cameras        	| 2         |
| J4    | Tracking Camera       	| 1         |


### 4 High-Resolution + Tracking

| Port  | Camera                	| Camera ID |
|------ |---------------------------|-----------|
| J2    | High-Resolution 4k RGB    | 0         |
| J3    | Empty                 	| N/A       |
| J4    | Tracking Camera       	| 1         |

### 5 TOF + Tracking

| Port  | Camera                	| Camera ID |
|------ |---------------------------|-----------|
| J2    | empty                 	| N/A       |
| J3    | TOF time-of-flight    	| 0         |
| J4    | Tracking Camera       	| 1         |

### 6 High-Resolution + TOF + Tracking

| Port  | Camera                	| Camera ID |
|------ |---------------------------|-----------|
| J2    | High-Resolution 4k RGB    | 0         |
| J3    | TOF time-of-flight    	| 1         |
| J4    | Tracking Camera       	| 2         |

### 7 TOF + Stereo + Tracking

This is the default configuration for the Seeker Drone

| Port  | Camera                	| Camera ID |
|------ |---------------------------|-----------|
| J2    | TOF time-of-flight    	| 2         |
| J3    | Stereo Cameras        	| 1         |
| J4    | Tracking Camera       	| 0         |

### 8 High-Resolution Only

| Port  | Camera                	| Camera ID |
|------ |---------------------------|-----------|
| J2    | High-Resolution 4k RGB    | 0         |
| J3    | Empty                 	| N/A       |
| J4    | Empty                 	| N/A       |

### 9 TOF Only

| Port  | Camera                	| Camera ID |
|------ |---------------------------|-----------|
| J2    | Empty                 	| N/A       |
| J3    | TOF time-of-flight    	| 0         |
| J4    | Empty                 	| N/A       |

### 10 Stereo Only

| Port  | Camera                	| Camera ID |
|------ |---------------------------|-----------|
| J2    | Empty                 	| N/A       |
| J3    | Stereo Cameras        	| 0         |
| J4    | Empty                 	| N/A       |


## Configuration Files

Rarely, if ever, will you need to touch these configuration files manually. In most cases you can use the [voxl-configure-cameras](/configure-cameras/) script as described above.

## Compatible Image Sensors

| Sensor Module                                                                             | Sensor      | Resolution / FOV        | Description          | Minimum System Image |
|--:----------------------------------------------------------------------------------------|-------------|--:----------------------|--:-------------------|--:-------------------|
| [ModalAI MSU-M0024](https://modalai.com/M0024)                                            | Sony IMX214 | 4k                      | High-Resolution      | 2.0                  |
| [ModalAI MSU-M0025](https://modalai.com/M0025)                                            | Sony IMX214 | 4k                      | High-Resolution      | 2.0                  |
| [ModalAI MSU-M0026](https://modalai.com/M0026)                                            | Sony IMX377 | 4k                      | High-Resolution      | 2.0                  |
| [ModalAI MDK-M0061-1](https://www.modalai.com/products/m0061-1)                           | Sony IMX412 | 4k                      | High-Resolution      | 2.5.1                |
| [ModalAI Stereo Sensor](https://modalai.com/stereo-sensor)                                | OV7251      | 640x480 85.6º diagonal  | Stereo               | 2.0                  |
| [ModalAI MSU-M0014-1-01](https://modalai.com/tracking-sensor)                             | OV7251      | 640x480 166º diagonal   | Tracking             | 2.0                  |
| Sunny GP161C                                                                              | OV7251      | 640x480 85.6º diagonal  | Stereo               | 2.0                  |
| [ModalAI MSU-M0072-1-00](https://modalai.com/products/m0072) equivalent to Sunny MD102A   | OV7251      | 640x480 166º diagonal   | Tracking             | 2.0                  |
| [ModalAI MSU-M0040-1-01](https://www.modalai.com/products/voxl-dk-tof) PMD TOF LiteOn A65 | irs10x0c    | 224x172 110º horizontal | TOF (Time of Flight) | 2.3                  |



[Next: Docker on VOXL](/docker-on-voxl/){: .btn .btn-green }