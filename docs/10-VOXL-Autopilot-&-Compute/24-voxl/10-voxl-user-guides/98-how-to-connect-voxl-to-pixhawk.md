---
layout: default
title: How to Connect VOXL to Pixhawk
parent: VOXL User Guides
nav_order: 98
has_children: false
permalink: /how-to-connect-voxl-to-pixhawk/
---

# How to Connect VOXL to Pixhawk

VOXL can be connected to any Pixhawk via a telemetry port, not just the ModalAI Flight Core!

VOXL talks to the Pixhawk via UART at 912600 baud at 5V logic level.

You can use [MCBL-00008](/cable-datasheets/#mcbl-00008) for this connection.

Alternatively you can make a cable that connects VOXL [Port J12](https://docs.modalai.com/voxl-datasheet-connectors/#j12-blsp5-off-board-uart-esc) to one of Pixhawk's "telem" or "UART/I2C" ports. Make sure to configure the appropriate port in PX4's Mavlink parameters.

| VOXL J12 Header | Pixhawk "telem" or "UART/I2C" Port |
|:----------------|:-----------------------------------|
| Pin 2 (TX)      | Pin 3 (RX)                         |
| Pin 3 (RX)      | Pin 2 (TX)                         |
| Pin 5 (GND)     | Pin 6 (GND)                        |

For more details on Header J12 see the [voxl-datasheet](/voxl-datasheet/) page. Note this is the only header that runs at 5V logic level which matches the Pixhawk.

Pixhawk is a trademark of Lorenz Meier
