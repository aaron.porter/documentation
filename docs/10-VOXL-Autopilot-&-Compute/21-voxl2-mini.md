---
layout: default3
title: VOXL 2 Mini
nav_order: 21
has_children: true
permalink: /voxl2-mini/
parent: VOXL Autopilot & Compute
summary: VOXL 2 Mini is ModalAI's next-gen autonomous computing platform built around the Qualcomm Flight RB5 5G Platform using the QRB5165 processor. 
thumbnail: /voxl2-mini/m0104-thumbnail.png
buylink: https://www.modalai.com/products/voxl-2-mini
---

# VOXL 2 Mini
{: .no_toc }

VOXL 2 Mini is ModalAI's smallest version of it's [Blue UAS Framework](https://www.diu.mil/blue-uas) AI Autopilot which is a powerful companion computer with integrated flight controller and is built around the Qualcomm Flight RB5 5G Platform using the QRB5165 processor.


<a href="https://www.modalai.com/products/voxl-2-mini" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;">Support</button></a>

<img src="/images/voxl2-mini/m0104-hand-hero.jpg" alt="m0104-hand-hero" width="1280"/>
