---
layout: default
title: Flight Core v2 External Sensors
parent: Flight Core v2 User Guides
grand_parent: Flight Core v2
nav_order: 10
permalink: /flight-core-v2-external-sensors/
---

# Flight Core v2 External Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## GPS/Compass

### Hardware

Use `J10` for standard 6pin style GPS/compass assemblies.

| Pin # | Signal Name                                                        |
|-------|--------------------------------------------------------------------|
| 1     | 5VDC (other pins 3.3V, input or output if supplied at another pin) |
| 2     | EXT_GPS_UART_2W_TX                                                 |
| 3     | EXT_GPS_UART_2W_RX                                                 |
| 4     | EXT_GPS_I2C_SCL                                                    |
| 5     | EXT_GPS_I2C_SDA                                                    |
| 6     | GND                                                                |

### Software

See standard PX4 GPS/compass drivers.

## External SPI Based Sensors

### Hardware

An external SPI bus connected to `SPI6` is available on `J14`

![Flight Core v2](/images/flight-core-v2/m0087-external-spi.png)

| Pin # | Signal Name                |
|-------|----------------------------|
| 1     | VREG_3V3                   |
| 2     | SPI6_MISO_EXPANSION (3P3V) |
| 3     | SPI6_MOSI_EXPANSION (3P3V) |
| 4     | SPI6_SCK_EXPANSION  (3P3V) |
| 5     | SPI6_CS_EXPANSION_N (3P3V) |
| 6     |                            |
| 7     |                            |
| 8     |                            |
| 9     |                            |
| 10    |                            |
| 11    |                            |
| 12    | GND                        |

### Software

You can enable the external SPI device to run on startup here as required: [boards/modalai/fc-v2/init/rc.board_sensors](https://github.com/PX4/PX4-Autopilot/blob/main/boards/modalai/fc-v2/init/rc.board_sensors).

For example, and note the `-S` is capital to denote external IMU, also not specifying bus with `-b X` which causes the probe to fail:

```bash
# External SPI6 ICM-42688
icm42688p -S -R 12 start
```
