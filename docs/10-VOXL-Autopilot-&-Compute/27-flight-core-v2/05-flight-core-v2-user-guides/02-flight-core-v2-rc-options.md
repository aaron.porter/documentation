---
layout: default
title: Flight Core v2 RC Options
parent: Flight Core v2 User Guides
grand_parent: Flight Core v2
nav_order: 2
permalink: /flight-core-v2-rc-options/
---

# Flight Core v2 RC Options
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Spektrum RC

### Hardware

![Flight Core v2](/images/flight-core-v2/m0087-rc-spektrum.png)

You can connect a Spektrum RC like DSMX-SPM9645 or DSMX-SPM9745 using `J13` and [MCBL-00005](/cable-datasheets/#mcbl-00005)) or by following this pinout:

| J13 Pin # | Signal Name                                   |
|-------|-----------------------------------------------|
| 1     | VREG_3V3 (Spektrum Power)                     |
| 2     | USART6_TX                                     |
| 3     | SPEKTRUM RX (3.3V), SBus RX (3.3V), USART6_RX |
| 4     | GND                                           |

### Software

An open source PX4 driver is available at [src/drivers/rc_input](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/rc_input).

## SBUS RC

### Hardware

![Flight Core v2](/images/flight-core-v2/m0087-rc-sbus.png)

To connect a FrSky receiver, like the X8R, you can use [MCBL-000XX](/cable-datasheets/#mcbl-000XX) or by following this pinout:

| J13 Pin # | Signal Name                                   |
|-------|-----------------------------------------------|
| 1     | NC       |
| 2     | NC                                     |
| 3     | RX |
| 4     | GND                                           |

| J8 Pin # | Signal Name                                   |
|-------|-----------------------------------------------|
| 1     | 5P0V       |
| 2     | NC                                     |
| 3     | NC |
| 4     | NC                                           |

### Software

An open source PX4 driver is available at [src/drivers/rc_input](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/rc_input).
