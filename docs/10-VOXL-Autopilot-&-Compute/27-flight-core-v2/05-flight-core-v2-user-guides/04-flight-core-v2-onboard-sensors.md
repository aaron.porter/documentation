---
layout: default
title: Flight Core v2 Onboard Sensors
parent: Flight Core v2 User Guides
grand_parent: Flight Core v2
nav_order: 4
permalink: /flight-core-v2-onboard-sensors/
---

# Flight Core v2 Onboard Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

![Flight Core v2](/images/flight-core-v2/m0087-onboard-sensors.png)

## How to Configure

Locate `boards/modalai/fc-v2/init/rc.board_sensors`, modify and rebuild.

## IMUs

### Hardware

- IMU1 - ICM-42688p (U14, SPI1)
- IMU2 - ICM-42688p (U11, SPI2)

### Software

See [src/drivers/imu/invensense/icm42688p](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/imu/invensense/icm42688p)

## Barometers

### Hardware

- BARO1 - BMP388 (U5, I2C4, 0x76) - disabled by default
- BARO2 - ICP-20100 (U17, I2C4, 0x63) - default

### Software

See [src/drivers/barometer/bmp388](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/barometer/bmp388)

Note: ICP-20100 not ported from VOXL2 yet.

## Magnetometer

### Hardware

- MAG1 - BMM150 (U13, I2C4, 0x10) - disabled by default and not expected to be usable. Will be removed in future builds.

### Software

See [src/drivers/magnetometer/bosch/bmm150](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/magnetometer/bosch/bmm150)

### How to Enable to Run on Bootup

The `v1.13.2-0.1.1` release was optimized for m500 use case and has the magnetometer disabled by default.  To enable on bootup, create a file on the SD Card at `/etc/extras.txt` with `bmm150 -I start` as contents.

As an example, here's using an SD Card reader on Ubuntu:

```
 travis@travis-18 > / > cd media/travis 
 travis@travis-18 > /media/travis > ls
3266-3639
 travis@travis-18 > /media/travis > cd 3266-3639 
 travis@travis-18 > /media/travis/3266-3639 > ls
dataman  log  uavcan.db  ufw
 travis@travis-18 > /media/travis/3266-3639 > mkdir etc
 travis@travis-18 > /media/travis/3266-3639 > cd etc 
 travis@travis-18 > /media/travis/3266-3639/etc > echo "bmm150 -I start" > extras.txt
```
