---
layout: default
title: VOXL 2 Mini Linux User Guide
parent: VOXL 2 Mini User Guides
nav_order: 21
permalink:  /voxl2-mini-linux-user-guide/
---

# VOXL 2 Mini Linux User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# Interfaces

## System Image 1.7+

- VOXL2 Mini SDK 1.1.X (`in development`)

<img src="/images/voxl2-mini/m0104-linux-user-guide-V1.7.jpg" alt="m0104-linux-user-guide-V1.7" width="1280"/>

### apps_proc Interfaces

| Protocol | Connector                                                         | Device         | Processor | Example |
|----------|-------------------------------------------------------------------|----------------|-----------|---------|
| UART     | [J10](/voxl2-mini-connectors/#j10---external-uart)                | /dev/ttyHS0    | apps_proc | [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) |
| SPI      | [J6](/voxl2-mini-connectors/#j6---camera-group-0-specific-pinout) | /dev/spidev0.0 | apps_proc | [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) |
| SPI      | [IMU1](/voxl2-mini-onboard-sensors/), ICM-42688p                  | /dev/spidev3.0 | apps_proc | [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) |
| I2C      | [J7](/voxl2-mini-connectors/#j7---camera-group-1-specific-pinout) | /dev/i2c-0     | apps_proc | [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) |

### slpi_proc Interfaces

TODO

## System Image 1.6

- VOXL2 Mini SDK 1.0.0 (`voxl2-mini_SDK_1.0.0.tar.gz`)

<img src="/images/voxl2-mini/m0104-linux-user-guide-all.jpg" alt="m0104-linux-user-guide-all" width="1280"/>
