---
layout: default
title: VOXL 2 Onboard Sensors
parent: VOXL 2 User Guides
nav_order: 4
permalink:  /voxl2-user-guides-onboard-sensors/
---

# VOXL 2 Onboard Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## IMU0 (U16)

### Hardware 

- ICM-42688p
- DSP - SSC QUP5

### Software

#### Checking slpi_proc IMU Status

The `px4-listener sensor_accel` and `px4-listener sensor_gyro` commands will provide status of this IMU.

For example:

```
px4-listener sensor_accel

TOPIC: sensor_accel
 sensor_accel_s
	timestamp: 50136333  (0.002592 seconds ago)
	timestamp_sample: 50136045  (288 us before timestamp)
	device_id: 2490378 (Type: 0x26, SPI:1 (0x00))
	x: -0.3328
	y: 0.2801
	z: 9.7995
	temperature: 32.9937
	error_count: 0
	clip_counter: [0, 0, 0]
```

```
px4-listener sensor_gyro

TOPIC: sensor_gyro
 sensor_gyro_s
	timestamp: 55642109  (0.001741 seconds ago)
	timestamp_sample: 55641871  (238 us before timestamp)
	device_id: 2490378 (Type: 0x26, SPI:1 (0x00))
	x: 0.0205
	y: -0.0528
	z: -0.0229
	temperature: 33.2654
	error_count: 0
```

#### PX4 Driver

Please see driver at [icm42688p](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/imu/invensense/icm42688p)

#### Setting IMU orientation

This is currently set in a startup script [here](https://github.com/modalai/px4-firmware/blob/voxl-dev/boards/modalai/rb5-flight/voxl-px4.config#L70)

## IMU1 (U17)

### Hardware 

- ICM-42688p
- apps_proc - /dev/spidev3.0

### Software

#### Checking apps_proc IMU Status

This IMU doesn't interface with PX4 but instead intended for the apps_proc.  To view status:

- Run `voxl-configure-imu` with the factory settings options
- Run `voxl-inspect-imu imu_apps`

```
voxl-inspect-imu imu_apps

Acc in m/s^2, gyro in rad/s, temp in C
gravity | accel_x accel_y accel_z | gyro_x  gyro_y  gyro_z |  Temp |
   9.38 |  0.93    0.16    -9.34  |  0.003  -0.012  -0.017 | 28.84 |
```

#### Driver

Please see [qrb516-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-imu-server)

Please see [voxl-inspect-imu](https://docs.modalai.com/voxl-inspect-imu/) as well.

## BARO (U19)

### Hardware 

- ICP-10100
- DSP - SSC QUP4
- Address: 0x63h

### Software

#### Checking Baro Status

This barometer interfaces with PX4 and the `px4-listener sensor_baro` command can be used to view its status:

```
px4-listener sensor_baro

TOPIC: sensor_baro
 sensor_baro_s
	timestamp: 166216724  (0.008201 seconds ago)
	timestamp_sample: 166216722  (2 us before timestamp)
	device_id: 10249001 (Type: 0x9C, I2C:5 (0x63))
	error_count: 0
	pressure: 1019.6817
	temperature: 30.3849
```

#### PX4 Driver

Please see [icp10100](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/barometer/icp10100) driver.
