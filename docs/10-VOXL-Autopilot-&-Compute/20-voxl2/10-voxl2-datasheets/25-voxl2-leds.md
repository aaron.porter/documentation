---
layout: default
title: VOXL 2 LEDs
parent: VOXL 2 Datasheets
nav_order: 25
permalink: /voxl2-leds/
---

# VOXL 2 LEDs
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

<img src="/images/voxl2/m0054-leds.jpg" alt="m0054-leds" width="1280"/>

## D1 - Power Good LED (Green)

| State          | Description  |
|---             |---           |
| Green          | indicates 3P3 VREG and (emulated) VBAT are good (using AND logic) |
| Off            | indicates either 3P3 VREG or emulated VBAT are not good |

Green 

## DS1 - EFuse Status LED (RBG)

| State          | Description  |
|---             |---           |
| Green          | Power is good AND no faults |
| Red            | Fault condition present (Over/Under Voltage, Thermal) |
| Off            | No power to board |

**NOTE:** The blue channel is exposed via TP3 (which is the test point nearest to DS1, encircled in the graphic above).

## DS2 - User Controllable RGB LED

All 3 colors of the DS2 LED are active high ON.

| State          | Description  |
|---             |---           |
| Red            | GPIO_82 set high, 2.1Vf @5mA |
| Green          | GPIO_83 set high, 3.1Vf @5mA |
| Blue           | GPIO_84 set high,  3.0Vf @5mA |
| Off            | GPIO_82, GPIO_83 and GPIO_84 set low |

## DS4 - PM8150L RGB LED Driver

The PM8150L can drive this RGB LED.  To control:

```
# Green off/on
voxl2:~$ echo 0 > /sys/class/leds/green/brightness
voxl2:~$ echo 255 > /sys/class/leds/green/brightness

# Red off/on
voxl2:~$ echo 255 > /sys/class/leds/red/brightness
voxl2:~$ echo 0 > /sys/class/leds/red/brightness

# Blue off/on
voxl2:~$ echo 0 > /sys/class/leds/blue/brightness
voxl2:~$ echo 255 > /sys/class/leds/blue/brightness
```
