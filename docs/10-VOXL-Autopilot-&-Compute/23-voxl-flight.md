---
layout: default3
title: VOXL Flight
nav_order: 23
has_children: true
permalink: /voxl-flight/
parent: VOXL Autopilot & Compute
summary: VOXL Flight is an all-in-one board solution that combines a VOXL onboard computer and a Flight Core PX4 flight controller.
thumbnail: /voxl-flight/voxlflighttop.png
buylink: https://www.modalai.com/products/voxl-flight
---

# VOXL-Flight
{: .no_toc }

VOXL Flight is an all-in-one board solution that combines a VOXL onboard computer and a Flight Core PX4 flight controller.

<a href="https://www.modalai.com/products/voxl-flight" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/8/voxl-flight" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![voxl-flight](/images/voxl-flight/voxlflighttop.jpg)
