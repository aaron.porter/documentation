---
layout: default
title: VOXL Flight IMUs
parent: VOXL Flight Datasheets
nav_order: 20
permalink: /voxl-flight-datasheet-imus/
---

# VOXL Flight IMUs
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL Flight IMU Hardare Configuration

VOXL Flight has two IMUs connected to the snapdragon and three connected to the PX4 flight controller MCU.

In the VOXL-SDK, both snadragon IMUs are enabled and provide data through MPA, however IMU0 should be preferentially used since it is a newer and better performing part. Note this differs from the VOXL board which contains two of the same IMU. Configuring MPA services with voxl-configure-mpa will select the correct IMU based on board part number.

### Summary

#### VOXL Flight M0019

| Configuration                   | Part Number | Interface  |
|---------------------------------|-------------|------------|
| VOXL IMU0 (recommended)         | ICM-42688-P | VOXL SPI10 |
| VOXL IMU1                       | ICM-20948   | VOXL SPI1  |
| Flight Core IMU1                | ICM-20602   | PX4 SPI1   |
| Flight Core IMU2                | ICM-42688   | PX4 SPI2   |
| Flight Core IMU3 (likely DNI'd) | BMI-088     | PX4 SPI6   |


#### VOXL Flight M0069

| Configuration    | Part Number | Interface  |
|------------------|-------------|------------|
| VOXL IMU0        | ICM-42688-P | VOXL SPI10 |
| VOXL IMU1        | ICM-42688-P | VOXL SPI1  |
| Flight Core IMU1 | ICM-20602   | PX4 SPI1   |
| Flight Core IMU2 | ICM-42688   | PX4 SPI2   |

### VOXL Flight IMUs Top

![VOXL-Flight IMUs Top](/images/voxl-flight/voxl-flight/voxl-flight-imus-top.png)

### VOXL Flight IMUs Bottom

![VOXL-Flight IMUs Bottom](/images/voxl-flight/voxl-flight/voxl-flight-imus-bottom.png)

[Next: VOXL Flight Configuration](/voxl-flight-datasheet-configuration/){: .btn .btn-green }
