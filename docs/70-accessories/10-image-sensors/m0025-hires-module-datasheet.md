---
layout: default
title: M0025 IMX214 Module Datasheet
parent: Image Sensors
nav_order: 25
has_children: false
permalink: /M0025/
---
# VOXL Hi-res Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MSU-M0025picture.jpg](/images/other-products/image-sensors/MSU-M0025picture.jpg)

Plugs directly into VOXL®! A 4k High-resolution Camera based on the Sony IMX214 sensor. Enable AI video processing and video recording at 1080p30 or 4k30 using this camera.




## Specification

### M0025-1 8.5cm IMX214 100° FOV ([Buy Here](https://www.modalai.com/M0025))

| Specification | Value                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------|
| Sensor         | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter        | Rolling                                                                                             |
| Resolution     | 4208x3120                                                                                           |
| Framerate      | up to 60Hz                                                                                          |
| Lens Size      | 1/3.06"                                                                                             |
| Focusing Range | 5cm~infinity                                                                                        |
| Fov(DxHxV)     | ~100°                                                                                               |
| Weight         | <1g                                                                                                 |

### M0025-2 8.5cm IMX214 81° FOV 

| Specification | Value                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------|
| Sensor         | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter        | Rolling                                                                                             |
| Resolution     | 4208x3120                                                                                           |
| Framerate      | up to 60Hz                                                                                          |
| Lens Size      | 1/3.06"                                                                                             |
| Focusing Range | 5cm~infinity                                                                                        |
| Focal Length   |                                                                                                     |
| F Number       |                                                                                                     |
| Fov(DxHxV)     | 81.3° x 69.0° x 54°                                                                                 |
| TV Distortion  |                                                                                                     |
| Weight         | <1g                                                                                                 |

### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File


#### 2D Diagram


## Module Connector Pinout 
[Pinout Images](https://storage.googleapis.com/modalai_public/modal_drawings/M0025pinoutImages.pdf)

## VOXL Integration

## VOXL SDK
### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor
### Indoor
![hires_in.jpg](/images/other-products/image-sensors/Samples/hires_in.jpg)
{:target="_blank"}

### Outdoor
![hires_out.jpg](/images/other-products/image-sensors/Samples/hires_out.jpg)
{:target="_blank"}

## Hardware Design Guidance