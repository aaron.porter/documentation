---
layout: default
title: M0161 IMX412 Low Light Image Sensor Datasheet
parent: Image Sensors
nav_order: 161
has_children: false
permalink: /M0161/
---

# VOXL Hi-res Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MSU-M0161picture.jpg](/images/other-products/image-sensors/MSU-M0161picture.jpg)

Built in the USA by ModalAI for VOXL® 2! A 4k High-resolution Camera based on the Sony Starvis IMX412 sensor. Enable video recording at 1080p30 or 4k30 using this image sensor module. NDAA '20 Section 848 compliant.

This module uses a micro-coax for increased mounting flexibility. It does require an adapter to connect with VOXL 2 such as M0173 or M0155 (optionally included).

## Specification

### Hi-resolution, 4k M0161 m12 IMX412 120° FOV ([Buy Here](https://www.modalai.com/products/msu-m0161))


| Specification | Value                                                                                          |
|----------------|------------------------------------------------------------------------------------------------|
| Sensor         | IMX412 [Datasheet](https://www.sony-semicon.com/files/62/flyer_security/IMX412-AACK_Flyer.pdf) |
| Shutter        | Rolling                                                                                        |
| Max Resolution | 7.857 mm (Type 1/2.3) 12.3 Mega-pixel                                                          |
| Framerate      | TBD                                                                                            |
| Lens Mount     | m12                                                                                            |
| Lens Part No.  | 27629F-16MAS-CM                                                                                |
| Focusing Range | TBD                                                                                            |
| Focal Length   | 2.7mm                                                                                          |
| F Number       | TBD                                                                                            |
| Fov(DxHxV)     | 120.4° x 93.5° x 146°                                                                          |
| TV Distortion  | TBD                                                                                            |
| Weight         | 7.5g                                                                                           |
| IR Filter      | Optional                                                                                       |

### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0161_IMX412_COAX_with_Lens.STEP)

#### 2D Diagram


## Module Connector Pinout for J1 ([S-H26-HR-DS-412](https://docs.modalai.com/micro-coax-user-guide/))

| Pin |	CAM Group Function                             | Pin |
|-----|------------------------------------------------|-----|
|  1  |	CCI I2C Bus, SDA                               |     |
|     |	1.2V DVDD (LDO)                                |  2  |
|  3  |	CCI I2C Bus, SCL                               |     |
|     |	1.2V DVDD (LDO)                                |  4  |
|  5  |	System GND                                     |     |
|     |	System GND                                     |  6	 |
|  7  |	CSI CLK P                                      |     |
|     |	1.2V DVDD (LDO)                                |  8  |
|  9  |	CSI CLK M                                      |     |
|     |	1.8V VDDIO (LDO)                               | 10  |
| 11  |	MIPI CSI High Speed Diff Pair, Data Lane 0, P  |     |
|     |	RESET_N Power down signal (Normally Unused)  | 12  |
| 13  |	MIPI CSI High Speed Diff Pair, Data Lane 0, M  |     |
| 14  |	MIPI CSI High Speed Diff Pair, Data Lane 1, P  |     |
|     |	MCLK, Buffered from VOXL 2, 1.8V               | 15  |
| 16  |	MIPI CSI High Speed Diff Pair, Data Lane 1, M  |     |
|     |	System GND                                     | 17  |
| 18  |	System GND                                     |     |
|     |	Sensor 2.8V AVDD (LDO)                         | 19  |
| 20  |	MIPI CSI High Speed Diff Pair, Data Lane 2, P  |     |
|     |	Sensor Sync Signal                             | 21  |
| 22  |	MIPI CSI High Speed Diff Pair, Data Lane 2, M  |     |
|     |	System GND | 23  |
| 24  |	MIPI CSI High Speed Diff Pair, Data Lane 3, P  |     |
|     |	3.8V Primary "Phone" Power (mimics nominal 1S) | 25  |
| 26  |	MIPI CSI High Speed Diff Pair, Data Lane 3, M  |     |

## VOXL 2 Compatible Cables and Interfaces

| Cable      | Adapter Requirements               |
|------------|------------------------------------|
| MCBL-00084 | [M0173](/M0173) or [M0155](/M0155) |


## VOXL SDK

SDK 1.3.0 and greater

More information [here](/voxl2-camera-configs/)

### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor
### Indoor
### Outdoor

## Hardware Design Guidance