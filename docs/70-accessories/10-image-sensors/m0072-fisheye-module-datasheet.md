---
layout: default
title: M0072 Fisheye Module Datasheet
parent: Image Sensors
nav_order: 72
has_children: false
permalink: /M0072/
---

# VOXL Tracking Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MSU-M0072picture.jpg](/images/other-products/image-sensors/MSU-M0072picture.jpg)

Replace the stereo image sensors used on VOXL to enable fisheye stereo.

Leveraging the industry's smallest global shutter pixel, the black and white OV7251 is capable of capturing VGA (640x480) resolution video at 120 frames per second (fps), QVGA (320x240) at 180 fps with binning, and QQVGA (160x120) at 360 fps with binning and skipping. The OV7251's high frame rates make it an ideal solution for low-latency machine vision applications.
* Molex connector, same pin-out as MD102A

Compatible With: 
* Flex cable MFPC-M0010
*Flex cable MPFC-M0008


## Specification

### MSU-M0072-1-01 OV7251 166° FOV ([Buy Here](https://www.modalai.com/products/m0072))


| Specification | Value                                                                                |
|----------------|--------------------------------------------------------------------------------------|
| Sensor         | OV7251 [Datasheet](https://www.ovt.com/wp-content/uploads/2022/01/OV7251-PB-v1.9-WEB.pdf) |
| Shutter        | Global                                                                               |
| Resolution     | 640x480                                                                              |
| Framerate      | 30,60,90Hz implemented on VOXL, sensor supports up to 120Hz                           |
| Data formats   | B&W 8 and 10-bit                                                                     |
| Lens Size      | 1/3.06"                                                                              |
| Focusing Range | 5cm~infinity                                                                         |
| Focal Length   | 0.83mm                                                                               |
| F Number       | 2.0                                                                                  |
| Fov(DxHxV)     | 166° x 133° x 100°                                                                   |
| TV Distortion  | -20.77%                                                                              |

### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File
TBD
#### 2D Diagram
[Download](/images/other-products/image-sensors/M0072-2D.jpg)

## Module Connector Pinout 

| Pin # | Signal Name            | Pin # | Signal Name            |
|-------|------------------------|-------|------------------------|
| 1     | DGND                   | 2     | XCLK                   |
| 3     | DGND                   | 4     | XSHUTDOWN              |
| 5     | STROBE                 | 6     | DGND                   |
| 7     | MDP                    | 8     | MDN                    |
| 9     | DGND                   | 10    | MCP                    |
| 11    | MCN                    | 12    | DGND                   |
| 13    | ULPN                   | 14    | FSIN                   |
| 15    | NC                     | 16    | DGND                   |
| 17    | SI0_D                  | 18    | SI0_C                  |
| 19    | DGND                   | 20    | D0VDD_1.8V             |
| 21    | NC                     | 22    | DGND                   |
| 23    | AVDD_2.8V              | 24    | AGND                   |


## VOXL Integration
SDK 0.7 and greater

More information [here](/voxl2-camera-configs/)

| Cable              | Adapter Requirements                                      |
|--------------------|-----------------------------------------------------------|
| [M0008-1](/M0008/) | [M00076](/M0076/) or [M0084](/M0084/) or [M0135](/M0135/) |
| [M0010-1](/M0010/) | [M00076](/M0076/) or [M0084](/M0084/) or [M0135](/M0135/) |
| [M0109-1](/M0109/) | None                                                      |


## VOXL SDK
### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor

### Indoor

![tracking_in.png](/images/other-products/image-sensors/Samples/tracking_in.png)
{:target="_blank"}

### Outdoor

![tracking_out.png](/images/other-products/image-sensors/Samples/tracking_out.png)
{:target="_blank"}

## Hardware Design Guidance