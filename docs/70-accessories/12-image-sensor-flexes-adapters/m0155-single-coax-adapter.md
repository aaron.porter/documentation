---
layout: default
title: M0155 Single Coax Adapter
parent: Image Sensor Flex Cables and Adapters
nav_order: 155
has_children: false
permalink: /M0155/
---

# M0155 VOXL 2 Single Image Sensor Micro-coax Adapter
{: .no_toc }

## Specification



Description TO DO

### Pin Out VOXL2-side



### Pin Out Mating-side



## Technical Drawings

### 3D STEP File

[M0155.step](https://storage.googleapis.com/modalai_public/modal_drawings/M0155.step)

### 2D Diagrams

