---
layout: default
title: M0173 VOXL 2 Starling Front End
parent: Image Sensor Flex Cables and Adapters
nav_order: 173
has_children: false
nav_exclude: false
permalink: /M0173/
---

# M0173 VOXL 2 Starling 2 Front End
{: .no_toc }

M0173 Serves as a breakout for Camera Groups J6 and J7 on VOXL 2. It exposes 4 COAX camera connectors for use with M0166 Tracking Cameras or M0161 IMX412 Hires cameras. It also exposes a connector for use with a single PMD TOF sensor, and a connector for attaching an M0157 Lepton plus rangefinder board.

[VOXL 2 Coax Camera Bundle](/voxl2-coax-camera-bundles/)

---
## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

### VOXL SDK Requirements

VOXL SDK 1.3.0+ is required.

### Hardware Requirements

Only compatible with [VOXL 2](/voxl2/)

## New Kernel Configuration for M0173

The `D0014/D0012` drone family uses the new kernel `config 1` (as opposed to `config 0`).  See `/sys/module/voxl_platform_mod/parameters/config` to verify proper kernel config.

## Features

### VOXL2 Driven Image Sensor Synchronization

In the D0014/D0012 architecture using the M0173 breakout, the following occurs for the tracking sensors:

- The [voxl-fysnc-mod](https://gitlab.com/voxl-public/system-image-build/meta-voxl2/-/tree/qrb5165-ubun1.0-14.1a/recipes-kernel/voxl-fysnc-mod) ships in SDK 1.3+ and defaults to use GPIO 109, 30Hz, and disabled.
- When a C26/C27/C8 image sensor configurations is setup using `voxl-configure-cameras`, the config file at `/etc/modalai/voxl-camera-server.conf` is setup with:

```
...
"fsync_en":	true,
"fsync_gpio":	109,
...
```

- When camera server runs, it will use the `voxl-fsync-mod` params exposed to the file system at `/sys/module/voxl_fsync_mod/parameters`, for example `enabled`, `gpio_num`, and enable the sync pulse at the default 30Hz.
- When a C26/C27/C8 image sensor configurations is setup using `voxl-configure-cameras`, the `AR0144` drivers are copied into `/usr/lib/camera` that are setup for slave mode and will capture sychronized frames at the rate defined (note: the FPS should also be configured in `/etc/modalai/voxl-camera-server.conf` accordinly to ensure proper register settings are loaded)

[![D0014-M0173.jpg](/images/d0014/D0014-M0173.jpg)](/images/d0014/D0014-M0173.jpg)

### Downward Range Finder and FLIR Lepton

In the D0014/D0012 architecture using the M0173 breakout:

- VL53L1CX Range Finder is accesible over `/dev/i2c-4`, see `/etc/modalai/voxl-rangefinder-server.conf` and `voxl-rangefinder`
- Not supported in SDK 1.3.0 but coming soon, FLIR Lepton is accesible over `/dev/i2c-4` and `/dev/spidev0.0`, see `/etc/modalai/voxl-lepton-server.conf` and `voxl-lepton-server`

[![D0014-M0157.jpg](/images/d0014/D0014-M0157.jpg)](/images/d0014/D0014-M0157.jpg)


## Connectors

### J1 - Front Tracking : ID0 (CSI0)

| Pin | Name     
| --- | ----       
| 1   | CCI_I2C0_SDA                       
| 3   | CCI_I2C0_SCL                     
| 5   | DGND                                         
| 6   | DGND                                               
| 7   | CSIO_CLK_CON_P                   
| 9   | CSIO_CLK_CON_N                    
| 10  | PM8009_VREG_L7_DOVDD_1P8                    
| 11  | CSIO_LANE0_CON_P                    
| 12  | GPIO_93_CAMO_RST_N                                        
| 13  | CSI0_LANE0_CON_N                                        
| 14  | CSIO_LANE1_CON_P     
| 15  | MCLK0_ID0                                        
| 16  | CSI0_LANE1_CON_N   
| 17  | DGND                                               
| 18  | DGND                                             
| 19  | PM8009_VREG_L5_AVDD_2P8
| 21  | TRACKR_TRIGGER_ID0                       
| 23  | DGND                      
| 25  | VPH_PWR                                  
| 2,4,8   | PM8009_VREG_L2_DVDD_1P2 


### J2 - Downward Tracking : ID6 (CSI0/Combo)

| Pin | Name     
| --- | ----       
| 1   | CCI_I2C1_SDA                    
| 3   | CCI_I2C1_SCL                     
| 5   | DGND                                         
| 6   | DGND                                               
| 7   | CSIO_LANE3_CON_P                   
| 9   | CSIO_LANE3_CON_N                   
| 10  | PM8009_VREG_L7_DOVDD_1P8                    
| 11  | CSIO_LANE2_CON_P                   
| 12  | GPIO_110_CCI_TIMER1                                        
| 13  | CSIO_LANE2_CON_N                                         
| 15  | MCLK0_ID6                                        
| 17  | DGND                                               
| 18  | DGND                                             
| 19  | PM8009_VREG_L5_AVDD_2P8
| 21  | TRACKR_TRIGGER_ID6                       
| 23  | DGND                                             
| 25  | VPH_PWR    
| 2,4,8   | PM8009_VREG_L2_DVDD_1P2 


### J3 - Front T0F: ID3 (CSI3)

| Pin | Name                         |
| --- | ---------------------------- |
| 1   | PM8009_VREG_L6_AVDD_2P8
| 2   | VPH_PWR
| 3   | PM8009_VREG_L7_DOVDD_1P8
| 4   | VREG_3P3V_LOCAL
| 5   | PM8009_VREG_L1_DVDD_1P05
| 6   | GPIO_92_CAMI_RST_N
| 7   | PM8009_VREG_L2_DVDD_1P2
| 8   | DGND
| 9   | DGND
| 10  | GPIO_4_CAM1_SPI1_MISO
| 11  | CSI3_CLK_CON_P
| 12  | GPIO_5_CAM1_SPI1_MOSI
| 13  | CSI3_CLK_CON_N
| 14  | GPIO_6_CAM1_SPI1_CLK
| 15  | CSI3_LANE0_CON_P
| 16  | GPIO_7_CAMI_SPI1_CS
| 17  | CSI3_LANE0_CON_N
| 18  | DGND
| 19  | CSI3_LANE1_CON_P
| 20  | GPIO_114_CCI_ASYNC_IN
| 21  | CSI3_LANE1_CON_N
| 22  | GPIO_100_CAM_SHARED_RST_N
| 23  | DGND
| 24  | CCI_I2C3_SDA
| 25  | CSI3_LANE2_CON_P
| 26  | CCI_I2C3_SCL
| 27  | CSI3_LANE2_CON_N
| 28  | CCI_I2C2_SDA
| 29  | CSI3_LANE3_CON_P
| 30  | CCI_I2C2_SCL
| 31  | CSI3_LANE3_CON_N
| 32  | DGND
| 33  | DGND
| 34  | GPIO_97_MCLK3
| 35  | GPIO_111_CCI_TIMER2
| 36  | GPIO_96_MCLK2
| 37  | VREG_3P3V_LOCAL
| 38  | VREG_S4A_1P8
| 39  | VPH_PWR
| 40  | TOF_SYNC_ID3 
| 43  | VDC_5V_LOCAL
| 44  | VDC_5V_LOCAL
    


### J4 - Front Hires : ID1 (CSI1)

| Pin | Name     
| --- | ----       
| 1   | CCI_I2C1_SDA                    
| 3   | CCI_I2C1_SCL                     
| 5   | DGND                                         
| 6   | DGND                                               
| 7   | CSI1_CLK_CON_P                   
| 9   | CSI1_CLK_CON_N                    
| 10  | PM8009_VREG_L7_DOVDD_1P8                    
| 11  | CSI1_LANE0_CON_P                    
| 12  | GPI0_113_CCI_TIMER4                                        
| 13  | CSI1_LANE0_CON_N                                        
| 14  | CSI1_LANE1_CON_P     
| 15  | GPIO95_MCLK1                                        
| 16  | CSI1_LANE1_CON_N   
| 17  | DGND                                               
| 18  | DGND                                             
| 19  | PM8009_VREG_L5_AVDD_2P8
| 20  | CSI1_LANE2_CON_P 
| 21  | HIRES_SYNC_ID1
| 22  | CSI1_LANE2_CON_N 
| 23  | DGND  
| 24  | CSI1_LANE3_CON_P 
| 25  | VPH_POWER 
| 26  | CSI1_LANE3_CON_N                                                                  
| 2,4,8   | PM8009_VREG_L2_DVDD_1P2

### J5 - Downward Hires : ID2 (CSI2)

| Pin | Name     
| --- | ----       
| 1   | CCI_I2C2_SDA                    
| 3   | CCI_I2C2_SCL                     
| 5   | DGND                                         
| 6   | DGND                                               
| 7   | CSI2_CLK_CON_P                   
| 9   | CSI2_CLK_CON_N                    
| 10  | PM8009_VREG_L7_DOVDD_1P8                    
| 11  | CSI2_LANE0_CON_P                    
| 12  | GPI092_CAMI_RST_N                                        
| 13  | CSI2_LANE0_CON_N                                        
| 14  | CSI2_LANE1_CON_P     
| 15  | GPIO96_MCLK2                                        
| 16  | CSI2_LANE_1_CON_N   
| 17  | DGND                                               
| 18  | DGND                                             
| 19  | PM8009_VREG_L6_AVDD_2P8
| 20  | CSI2_LANE2_CON_P 
| 21  | HIRES_SYNC_ID2
| 22  | CSI2_LANE2_CON_N 
| 23  | DGND  
| 24  | CSI2_LANE3_CON_P 
| 25  | VPH_POWER 
| 26  | CSI1_LANE3_CON_N    
| 2,4,8   | PM8009_VREG_L2_DVDD_1P2

### J6 - SM08B-SRSS-TB

| Pin | Name    
| --- | ----       
| 1   | VREG_AVDD_2P8_FLIR     
| 2   | FLIR_SPI_SCK_2P8               
| 3   | FLIR_SPI_CS_N_2P8      
| 4   | FLIR_SPI_MISO_2P8                
| 5   | FLIR_I2C_SCL_2P8V                                         
| 6   | FLIR_I2C_SDA_2P8V                                               
| 7   | CLK_25M_FLIR_2P8V 
| 8   | GND    