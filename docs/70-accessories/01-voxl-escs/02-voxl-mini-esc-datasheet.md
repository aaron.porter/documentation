---
layout: default
title: VOXL Mini 4-in-1 ESC Datasheet
parent: VOXL ESCs
nav_order: 2
permalink: /voxl-mini-esc-datasheet/
---

# VOXL Mini 4-in-1 ESC Datasheet
{: .no_toc }

---

## Hardware Overview

This Mini ESC is targeted for aerial vehicles under 750g. For vehicles up to 1500g, please use the [VOXL 4-in-1 ESC](/modal-esc-datasheet/).

![m0129-labels.jpg](/images/modal-esc/m0129/m0129-labels.jpg)

## Dimensions

[M0129 VOXL Mini 4-in-1 ESC 3D CAD](https://storage.googleapis.com/modalai_public/modal_drawings/M0129_ESC_4_IN_1_MICRO_20230222_FINAL.stp)
![m0129-dimensions.jpg](/images/modal-esc/m0129/m0129-dimensions.png)


## Specifications

|                        | Details                                                                                         |
|------------------------|-------------------------------------------------------------------------------------------------|
| Power Input            | 2-4S Lipo (6-18V)                                                                               |
|                        |                                                                                                 |
| Main Power Output      | 3.8V (M0129-3) or 5.0V (M0129-5) @ 5A - (select correct version)                                |
| AUX Power Output       | 3.3V or 5.0V @ 500mA (default 3.3V, SW-controlled)                                              |
|                        |                                                                                                 |
| Features               | Open-loop control (set desired % power)                                                         |
|                        | Closed-loop RPM control (set desired RPM), used in PX4 driver                                   |
|                        | Regenerative braking                                                                            |
|                        | Smooth sinusoidal spin-up                                                                       |
|                        | Tone generation using motors                                                                    |
|                        | Real-time RPM, temperature, voltage, current feedback via UART                                  |
| Communications         | Supported by VOXL 2, VOXL 2 Mini, VOXL Flight, VOXL and Flight Core                             |
|                        | Dual Bi-directional UART up to 2Mbit/s (3.3VDC logic-level)                                     |
|                        |                                                                                                 |
| Connectors             | 4-pin JST GH for UART communication, solder pads for 3.8V output                                |
|                        |                                                                                                 |
| Hardware               | MCU : STM32F051K86 @ 48Mhz, 64KB Flash                                                          |
|                        | Mosfet Driver : MP6531                                                                          |
|                        | Mosfets	: CSD17575Q3 (N-channel)                                                               |
|                        | Current Sensing : 0.5mOhm + INA186 (total current only)                                         |
|                        | ESD Protection : Yes (on UART and PWM I/O)                                                      |
|                        | Temperature Sensing : Yes                                                                       |
|                        | On-board Status LEDs : Yes                                                                      |
|                        | Weight (no wires) : 5.87g                                                                       |
|                        | Motor Connectors: N/A (solder pads)                                                             |
| PX4 Integration        | Supported in PX4 1.12 and higher                                                                |
|                        | Available [here](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/actuators/voxl_esc)                      |
| Resources              | [Manual](/modal-esc-v2-manual/)                                                                 |
|                        | [PX4 Integration User Guide](/modal-esc-px4-user-guide/)                                        |

## Connector Information and Pin-outs
**UART Connector J1**
* Used for UART communication with the ESC
* Connector on board : BM04B-GHS-TBT
* Mating connector : GHR-04V-S
* Pre-crimped wires : (Digikey) AGHGH28K152 or similar
* 3.3V signals (5.0V input is acceptable)

| Pin >      |     1    |    2    |    3    |    4    |
| :------:   | :------: | :-----: | :-----: | :-----: |
| Function > |    N/C   | UART RX (IN) | UART TX (OUT) |   GND   |

## Neopixel LED Support
![m0129-neopixel.jpg](/images/modal-esc/m0129/m0129-neopixel.jpg)
- Single Neopixel RGB LED outputs is available, up to 32 LEDs
- Use test point labeled ⬇️ (pointing into center of the board) in the AUX UART section of the ESC (AUX UART is not used)
- The I/O pin is connected to ESC ID0 `PB6` (3.3V levels)
- ➕ output provides 3.3V output for the LED array
- Test tools: [voxl-esc-neopixel-test.py](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc/-/blob/master/voxl-esc-tools/voxl-esc-neopixel-test.py)
- Integration with PX4 is done via `voxl_esc` driver and `modal_io_bridge` 

## PWM Inputs / Outputs
![m0129-pwm.jpg](/images/modal-esc/m0129/m0129-pwm.jpg)
- Four independent PWM input / output pins are available, one pin per ESC channel
- Use test points G (GND), A0, A1, A2, A3
- PWM pin A0 is connected to ESC ID0, A1 -> ESC ID1, and so on (3.3V levels)
- ⚠️*Solder carefully to test points and use strain relief to avoid damaging the pads*
- PWM input enabled in ESC firmware `39.13` or later, PWM output in `39.18` or later
- Operating mode of PWM pins
   - Default mode of PWM pins is PWM input. In absence of UART communication, PWM pins can be used to control the ESC power using standard PWM signal (1-2ms)
   - Upon detecting any UART communication by ESC, PWM input is disabled until power reset
   - PWM output can be enabled by sending specific message to the ESC via UART interface
- PWM output mode specifications
   - 3 frequency modes (50hz, 200hz, 400hz)
   - enable or disable timeout (0.5s). If timeout is enabled, PWM output will be disabled (set to low state) after 0.5s of no commands
   - output range 0-2200 us with 0.05us resolution, special value of 2201us will force HIGH state (can be used as GPIO)
- Test tools: [voxl-esc-pwm.py](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc/-/blob/dev/voxl-esc-tools/voxl-esc-pwm.py)
- Integration with PX4 is done via `voxl_esc` px4 driver and `modal_io_bridge` . [test app](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/blob/master/tools/voxl-send-esc-pwm-cmd.c)

## GPIO
- Available in ESC firmware `39.19` or later
- Test tools: [voxl-esc-gpio.py](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc/-/blob/dev/voxl-esc-tools/voxl-esc-gpio.py)


| Signal Name     | MCU Pin  | Function                                |
|-----------------|----------|-----------------------------------------|
| AUX_VREG_ENABLE | CH0 PC13 | AUX 3.3 / 5.0V Regulator Power Control  |
| AUX_VREG_ADJUST | CH0 PC14 | AUX 3.3 / 5.0V Regulator Voltage Select (disabled) |
