---
layout: default
title: Misc Software
parent: Deprecated
nav_order: 98
search_exclude: true
has_children: false
permalink: /misc-software/
---

# Misc Software
{: .no_toc }

---


## This information is deprecated.
{: .no_toc }

---


Please refer to [VOXL SDK](/voxl-sdk/) for our current software bundle.


### Core Components and Tools

| Feature                | Description                                                                                                                    | Binary                                                                                                                                           | Source Code                                                                                                                                                                                                                                                                                                                              | Usage Instructions                                                                                                                                       |
|:-----------------------|:-------------------------------------------------------------------------------------------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------|
| Datasheet              |                                                                                                                                |                                                                                                                                                  | [VOXL Datasheet](/docs/datasheets/voxl-datasheet.md) {::nomarkdown} <br> {:/} [Snapdragon 820 Datasheet](https://developer.qualcomm.com/download/sd820e/qualcomm-snapdragon-820e-processor-apq8096sge-device-specification.pdf) {::nomarkdown} <br> {:/} [Snapdragon 820 docs](https://developer.qualcomm.com/hardware/apq-8096sg/tools) |                                                                                                                                                          |
| Yocto Compiler         |                                                                                                                                | gcc-4.8-multilib                                                                                                                                 |                                                                                                                                                                                                                                                                                                                                          | [Build Yocto Image](../os-build-guides/README.md)                                                                                                        |
| meta-debian            |                                                                                                                                |                                                                                                                                                  | [Source](https://gitlab.com/voxl-public/voxl-build/blob/master/)                                                                                                                                                                                                                                                                         | [README](https://gitlab.com/voxl-public/voxl-build/blob/master/README.md)                                                                                |
| cross-compiler         | A Docker environment to compile for CPU and Hexagon DSP                                                                        | https://gitlab.com/voxl-public/voxl-docker                                                                                                       | https://gitlab.com/voxl-public/voxl-docker                                                                                                                                                                                                                                                                                               |                                                                                                                                                          |
| Emulator               | An Docker-based ARM emulator for the VOXL/Snapdragon 820 platform for off-target development and compiling for the CPU.        | [Binary Download](/docs/resources/downloads)                                                                                                     | https://gitlab.com/voxl-public/voxl-docker                                                                                                                                                                                                                                                                                               |                                                                                                                                                          |
| sDSP Compiler          |                                                                                                                                | Current: [Hexagon SDK v3.0](https://developer.qualcomm.com/download/hexagon/hexagon-sdk-v3-linux.bin) {::nomarkdown} <br> {:/} Planned: v3.4.2   | Not Available                                                                                                                                                                                                                                                                                                                            | [Example](https://docs.modalai.com/docs/resources/examples/)                                                                                             |
| aDSP Compiler          |                                                                                                                                | Current: [Hexagon SDK v3.1](https://developer.qualcomm.com/download/hexagon/hexagon-sdk-v3-1-linux.bin) {::nomarkdown} <br> {:/} Planned: v3.4.2 | Not Available                                                                                                                                                                                                                                                                                                                            | Coming Soon                                                                                                                                              |
| JTAG                   |                                                                                                                                | CPU: Planned {::nomarkdown} <br> {:/} DSP: Not Available                                                                                         | CPU: Planned {::nomarkdown} <br> {:/} DSP: Not Available                                                                                                                                                                                                                                                                                 |                                                                                                                                                          |
| Camera API (USB / UVC) |                                                                                                                                |                                                                                                                                                  |                                                                                                                                                                                                                                                                                                                                          | [Guide](https://docs.modalai.com/docs/user-guides/uvc-streaming/)                                                                                        |
| Camera API (MIPI)      |                                                                                                                                | Installed with image                                                                                                                             | [API](https://source.codeaurora.org/quic/la/platform/hardware/qcom/camera/tree/libcamera?h=LNX.LER.1.2)                                                                                                                                                                                                                                  | [Usage Guide](https://docs.modalai.com/docs/user-guides/camera-usage/)                                                                                   |
| Configure Wi-Fi        |                                                                                                                                | Installed with image                                                                                                                             | [Docs Source](https://gitlab.com/voxl-public/documentation/blob/master/docs/quickstarts/wifi-setup.md)                                                                                                                                                                                                                                   | [Wi-Fi Setup](https://docs.modalai.com/docs/quickstarts/wifi-setup/)                                                                                     |
| Configure LTE          |                                                                                                                                | [Binary Download](/docs/resources/downloads)                                                                                                     | [Docs Source](https://gitlab.com/voxl-public/documentation/blob/master/docs/user-guides/connect-to-lte.md)                                                                                                                                                                                                                               | [Usage Guide](https://docs.modalai.com/docs/user-guides/connect-to-lte/)                                                                                 |
| On-target Docker       | Run Docker on target <br>Ubuntu 14.04 tested, OS compatible with 3.18 kernel, requires modalai-1-6-0 or higher system image    |                                                                                                                                                  | [Gitlab](https://gitlab.com/voxl-public/poky/tree/modalai-sd820-jethro-docker)                                                                                                                                                                                                                                                           | [User Guide](https://docs.modalai.com/docs/user-guides/docker/) [README](https://gitlab.com/voxl-public/poky/blob/modalai-sd820-jethro-docker/README.md) |
| On-target Balena       | Run Balena app on target<br>Ubuntu 14.04 tested, OS compatible with 3.18 kernel, requires modalai-1-6-0 or higher system image |                                                                                                                                                  | [Gitlab](https://gitlab.com/voxl-public/poky/tree/modalai-sd820-jethro-docker-balena)                                                                                                                                                                                                                                                    | [README TBD](https://gitlab.com/voxl-public/poky/blob/modalai-sd820-jethro-docker-balena/README.md)                                                      |

### ModalAI Applications

ModalAI Developed Applications can be found [here](/docs/resources/downloads), often in source code. Some examples of the applications found are:

| Feature          | Description | Binary | Source Code | Usage Instructions | VOXL Versions Supported |
| ---              | ---         | ---    | ---         | ---                | ---                     |
| i2c_display     | | | | | modalai-1-3-0 and higher |
| ffmpeg/ffserver | | | | | modalai-1-3-0 and higher |
| opencv-3-4-6    | | | | | modalai-1-3-0 and higher |
| voxl-cam-manager| A fork of snap-cam-manager evolved for the VOXL platform | | | | modalai-1-3-0 and higher |
| voxl_imu | | | | | modalai-1-3-0 and higher |
| voxl-utils | | | | | modalai-1-3-0 and higher |
| voxl-python-3.6.9 | Scripts to compile and package Python 3.6.9 for the VOXL platform | | [Gitlab](https://gitlab.com/voxl-public/voxl-python-3.6.9) | | |
| voxl-camera-calibration | A ROS environment to calibrate stereo and fisheye cameras | | [Gitlab](https://gitlab.com/voxl-public/voxl-camera-calibration) | | |


### Qualcomm Proprietary Enablers

| Content | Binary | Source Code | Usage Instructions |
| ---     | ---    | ---         | ---                |
| Camera ISP (AAA) driver | | V4L2 exposed as open source to enable new sensors | |
| Camera Color Tuning ISP for new Sensor  | Not supported | Not supported | Not supported |
| Adreno GPU            | | [Freedreno Planned in Future](https://github.com/freedreno/mesa) | |
| Wi-Fi / Bluetooth Firmware | | | |
| VIO | | Open-source VIO alternates planned | |
| Depth from Stereo| | Open-source DFS alternates planned | |
| Snapdragon Navigator Flight Control| | PX4 as alternate | |

### Tested SNAV / MV Combinations

| SNAV Package | MV Package | Location | Notes |
| ---          | ---        | ---      | ---   |
| 1.2.59 | 1.1.9 | [SNAV](https://developer.qualcomm.com/downloads/qualcomm-navigator-v1259-8x96?referrer=node/34698), [MV](https://developer.qualcomm.com/download/machine-vision/machine-vision-sdk-1.1.9.8x96?referrer=node/34482) | Well tested, recommended |
| 1.3.0 (snav-modalai_1.3.0_8x96.ipk) | 1.2.8 | [Developer Website](https://developer.modalai.com/asset) | Defaults to IMU1 for better performance |
