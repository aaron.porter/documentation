---
layout: default
title:  HDMI Input Accessory Datasheet
search_exclude: true
parent: Deprecated
nav_order: 35
has_children: false
permalink: /hdmi-input-accessory-datasheet/
---

# HDMI Input Accessory Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Summary

Provides HDMI input into VOXL or VOXL Flight.

*** Note: ModalAi's HDMI accessory has been discontinued and is no longer available ***

[User Manual](/hdmi-input-accessory-manual/)

## Board Connections

SYSTEM

![FC-DK-R1](/images/voxl/voxl-acc-hdmi/voxl-acc-hdmi-system.png)

TOP

![voxl-acc-hdmi-top](/images/other-products/hdmi-usb-accessories/hdmi-input-top.png)

BOTTOM

![voxl-acc-hdmi-bottom](/images/other-products/hdmi-usb-accessories/hdmi-input-bottom.png)

## Specifications

### System Specification

- HDMI input connector for camera, MIPI connector output for VOXL/VOXL Flight
- Provides 1080p videa to VOXL/VOXL Flight at variable frame rates up to 60FPS in a named pipe
- Requires ModalAI System Image 2.6 or newer
- For B102 specifications, refer to [this link](https://auvidea.eu/download/manual/B10x_technical_reference_1.4.pdf)

### Physical Specification

System:

| Specification     | Value |
| ---                | --- |
| Size               | TODO |
| Weight             | TODO |

Auvidea PCB:

| Specification     | Value |
| ---                | --- |
| Size               | 27 x 42 mm |
| Weight             | TODO |
| Mounting           | 3x M2.5 mounting holes (do not use M3 screws) |
| Mount Hole Spacing | 22mm (h) x 21mm (v) |
