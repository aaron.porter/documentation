---
layout: default3
title: VOXL Flight Deck
nav_order: 20
has_children: true
parent: VOXL Dev Kits
permalink: /flight-deck/
thumbnail: /voxl-flight-deck/flight-deck.png
buylink: https://www.modalai.com/products/voxl-flight-deck-r1
summary: A fully assembled and calibrated flight kit. Pre-configured for GPS-denied navigation using Visual Inertial Odometry (VIO)
---

# VOXL Flight Deck
{: .no_toc }

A fully assembled and calibrated flight kit. Pre-configured for GPS-denied navigation using Visual Inertial Odometry (VIO).

<a href="https://www.modalai.com/products/voxl-flight-deck-r1" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/9/voxl" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![voxl-flight-deck](/images/voxl-flight-deck/flight-deck.jpg)


| Part Number                    | Description  |
|---                             |---           |
| `MDK-F0006-1-01`               | VOXL 2 Flight Deck with Microhard Modem Carrier Board and USB Hub |
| `MDK-F0006-1-02`               | VOXL 2 Flight Deck with Microhard Modem Carrier Board and USB Hub |
| `MDK-F0006-1-03`               | VOXL 2 Flight Deck with Microhard Modem Carrier Board and USB Hub |


