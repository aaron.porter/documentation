---
layout: default3
title: VOXL 2 Flight Deck
nav_order: 22
has_children: true
permalink: /voxl2-flight-deck/
parent: VOXL Dev Kits
thumbnail: /voxl2-flight-deck/voxl2-deck-hero.png
buylink: https://www.modalai.com/products/voxl-2-flight-deck
summary: ModalAI's most advanced fully assembled and calibrated flight kit to date
---

# VOXL 2 Flight Deck
{: .no_toc }

ModalAI's most advanced fully assembled and calibrated flight kit to date. Powered by VOXL 2, it combines the smallest single-board autopilot with 8 core processing from the Qualcomm QRB5165, as well a PX4 flight controller, advanced onboard AI and 5G connectivity. It comes pre-configured with Tracking, Stereo and Hi-Res optical sensors for GPS-Denied Navigation and Obstacle Avoidance. 

<a href="https://www.modalai.com/products/voxl-2-flight-deck" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/26/voxl-2" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![voxl2-flight-deck](/images/voxl2-flight-deck/voxl2-deck-hero.png)
