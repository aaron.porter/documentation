---
title: TFLite Guide
layout: default
parent: Custom VOXL Applications
has_children: true
nav_order: 35
permalink: /voxl-tflite-guide/
---

[LiteRT](https://ai.google.dev/edge/litert) (formerly TensorFlow Lite) is a powerful deep learning framework for embedded systems. VOXL supports this through the voxl-tflite-server. Please refer to the [voxl-tflite-server](/voxl-tflite-server/) documentation for more details.