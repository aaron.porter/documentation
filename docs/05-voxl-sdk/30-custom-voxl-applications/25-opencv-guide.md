---
title: OpenCV Guide
layout: default
parent: Custom VOXL Applications
has_children: false
nav_order: 25
permalink: /opencv/
---


# OpenCV
{: .no_toc }

ModalAI hosts a native, VOXL-tuned version of OpenCV in 32-bit and 64-bit. The repository containing the tools required to build the OpenCV package for VOXL is available on [Gitlab](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-opencv).

Some good examples of using OpenCV on VOXL:

 - [voxl-logger](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger/-/blob/master/tools/voxl-logger.cpp) and [voxl-replay](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger/-/blob/master/tools/voxl-replay.cpp) use OpenCV to save and load PNG files.

 - [voxl-tag-detector](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tag-detector) uses opencv to generate stereo image rectification maps, but uses a more optimized algorithm do do the actual undistortion.

 - [voxl-camera-calibration](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-camera-calibration) uses OpenCV to calculate camera lens parameters.



