---
title: Accessing VOXL Portal via. ADB
layout: default
parent: Misc. User Guides
has_children: true
nav_order: 65
permalink: /voxl-portal-via-adb/
---

# Accessing VOXL Portal via. ADB
{: .no_toc }

## Introduction

The [voxl-portal](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-portal) package provides an embedded webserver on VOXL. It enables camera inspection and other debug tools all via web browser interface at your VOXL’s IP address. See [here](/voxl-portal/) for more info.

By default, portal can be accessed by navigating to a drone's IP address in one's browser. This requires a network connection from the drone to the host PC via an IP link, like WiFi.

Starting with SDK 1.3.5, portal can be accessed using an ADB connection alone!

## Setup

In order to accomplish this, we can take use of ADB's port forwarding ability. Port forwarding is a networking technique that allows external device to access services on a private network. It works by directing incoming traffic on a specific port of a router or firewall to a particular device and port within the local network.

`adb forward` allows a user to forward TCP traffic on a port from the device (VOXL 2) to one's host PC.

voxl-portal's webserver runs on port 80, we can use `adb forward` in order to send TCP traffic to our host PC's network on port `8080` with the following:

```
adb forward tcp:8080 tcp:80
```

## Usage

Now that voxl-portal's TCP traffic is being forwarded to our host machine, we can navigate to our browser and go to:

http://localhost:8080/


![voxl portal via adb](/images/voxl-sdk/voxl-portal/voxl-portal-via-adb.png)

All of voxl-portal's pages can be accessed as usual:

![voxl portal via adb cameras](/images/voxl-sdk/voxl-portal/voxl-portal-via-adb-cameras.png)


Unfortunately, this port forwarding must be started each time the ADB server is run.

However, what we recommend is setting up an alias in your terminal emulator to automatically run this forwarding so you don't have to start it up each time.

Add the following to your `~/.bashrc` file (`~/.zshrc` for zsh)

```
alias voxl='adb wait-for-device && adb forward tcp:8080 tcp:80 && adb shell'
```

Restart your terminal and run `voxl`, you will both ADB onto your drone as well as enable the ability to access voxl-portal via. your browser without an external network connection.