---
title: MAVCam Manager
layout: default
parent: Cameras & Video
nav_order: 50
has_children: false
permalink: /voxl-mavcam-manager/
---

# MAVCam Manager

## Overview

[voxl-mavcam-manager](/mpa-camera-interface/) is a C++ code base that provides the interface between MPA camera streams and devices connected to VOXL 2 via MAVLink such as QGroundControl (QGC). This stream is leveraged from voxl-streamer and uses the rtsp stream created from said service.

## Source Code

[voxl-mavcam-manager](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavcam-manager)

## Setup

Video from voxl-camera-server will be streamed automatically to a GCS if the following services are configured and running:

1. [voxl-camera-server](/voxl-camera-server/)
2. [voxl-streamer](/voxl-streamer/)
3. [voxl-mavlink-server](/voxl-mavlink-server/)

Note: If you plan on NOT leveraging the `auto_ip` parameter to auto configure the ip address of the rtsp stream in mavcam manager, then voxl-mavlink-server does not need to be active during run of mavcam manager.

To setup the config file, the user can run the following:

`voxl-configure-mavcam-manager` - this will create a config file in /etc/modalai that contains the information to stream one singular camera to the QGC instance.

The config file has the following information in it:

```
 * This file contains configuration parameters for voxl-mavcam-manager.
 * 
 * mavcam_inputs:
 *      Array of inputs if one wishes to stream more than one camera using mavcam inspector
 *      Each entry has 4 inputs: 
 * Fields:\n\
 *      snapshot_pipe_name:       pipe to send snapshot command to (default hires_snapshot)
 *      video_record_pipe_name:   pipes to pull h264/h265 or raw frame data from for recording video
 *                                      to disk when the user clicks the record button in QGC
 *      default_uri:              this is the default uri to assume the rtsp stream is at
 *                                      Default value is rtsp://192.168.8.1:8900/live
 *      enable_auto_ip:           disable this to force the use of the default ip address
 *                                  when advertizing to the GCS. Otherwise we will use
 *                                  voxl-my-ip to auto-pick an ip address.
 * 
 * To change the pipe that the live rtsp video stream is pulled from, edit
 * /etc/modalai/voxl-streamer.conf instead. Other option is to stream another rtsp via the standalone feature.
```

So, for example, if the user wishes to have the ability to send both the tracking and hires video stream, the config file would look like the following:

```
/**
 * This file contains configuration parameters for voxl-mavcam-manager.
 * 
 * mavcam_inputs: 
 *      Array of inputs if one wishes to stream more than one camera using mavcam inspector
 *      Each entry has 4 inputs: 
 * Fields:
 *      snapshot_pipe_name:       pipe to send snapshot command to (default hires_snapshot)
 *      video_record_pipe_name:   pipes to pull h264/h265 or raw frame data from for recording video
 *                                      to disk when the user clicks the record button in QGC
 *      default_uri:              this is the default uri to assume the rtsp stream is at
 *                                      Default value is rtsp://192.168.8.1:8900/live
 *      enable_auto_ip:           disable this to force the use of the default ip address
 *                                  when advertizing to the GCS. Otherwise we will use
 *                                  voxl-my-ip to auto-pick an ip address.
 * 
 * To change the pipe that the live rtsp video stream is pulled from, edit
 * /etc/modalai/voxl-streamer.conf instead. Other option is to stream another rtsp via the standalone feature.
 *
 */
{
	"mavcam_inputs":	[{
                        "snapshot_pipe_name":	"hires_snapshot",
                        "video_record_pipe_name":	"hires_large_encoded",
                        "default_uri":	"rtsp://192.168.8.1:8900/live",
                        "enable_auto_ip":	false
                        }, {
                        "snapshot_pipe_name":   "tracking",
                        "video_record_pipe_name":       "tracking",
                        "default_uri":  "rtsp://192.168.8.1:8901/live",
                        "enable_auto_ip":       false
                        }]
}
```

Keep in mind that in order to stream both the tracking and the hires camera to qgc using mavcam manager, TWO instances of voxl-streamer must be running to create each rtsp endpoint. If the user wishes to send only one singular rtsp stream to the qgc instance and plans on using only the base instance of voxl-streamer, the user can turn `enable_auto_ip` to `true`, which will automatically set the uri correctly upon runtime of the service. If this is the case though, the user must have voxl-mavlink-server enabled as well as the voxl2 in SOFTAP mode so the IP address connected from the parent computer is that of one with a 192.168.8.X ip address.

Once the config file is setup correct, and the user has the correct voxl-streamer services running, the user can restart their voxl2 and upon start of the mavcam service, the user will have the ability to select or watch whichever stream they wish to view without having to put in the streams into QGC's section for `RTSP Stream to watch`. 

## How to view stream in QGC and take snapshots/video

In order to view the stream in QGC, the user just has to ensure that the voxl2 and the parent computer are on the same network.

From a QGC perspective, nothing needs to be done - the user should be able to see the rtsp streams out the gate on the instance. To select a specific stream, the user can use the settings camera option in the right side of qgc to select specific stream (gear icon on camera settings):

<img src="/images/voxl-sdk/voxl-mavcam-manager/camera-1-mavcam-manager.png" alt="camera-1-mavcam-manager" width="1280"/>

<img src="/images/voxl-sdk/voxl-mavcam-manager/camera-2-mavcam-manager.png" alt="camera-2-mavcam-manager" width="1280"/>

If the user wishes to take a snapshot and save it on both the disk of the voxl2 and the disk of the computer running the QGC instance, they just need to click the red button here:

<img src="/images/voxl-sdk/voxl-mavcam-manager/snapshot-red-button.png" alt="snapshot-red-button"/>

If the user wishes to save a hires video of the stream on disk of the voxl2, they can select the red button here:

<img src="/images/voxl-sdk/voxl-mavcam-manager/record-red-button.png" alt="record-red-button"/>

The ability to save the video on disk on the parent computer running QGC has NOT been implemented in the most recent stable version of QGC.

### Geo-tagging Snapshots

__Note:__ this feature is only available on VOXL 2

When GPS is being published via the [voxl-mavlink-server](/voxl-mavlink-server/), voxl-camera-server will geo-tag each snapshot captured with the latest GPS coordinates. The GPS data (latitude, longitude, altitude) will be saved in the JPEG Snapshot EXIF metadata. In order for this to occur, the user must have a valid GPS connected and actively publishing data from a MAVLink compatible flight controller, such as PX4 or ArduPillot.

You can verify GPS is being published with the [voxl-inspect-gps](/voxl-inspect-gps/) tool.

In order to view the EXIF metadata of the photo, the user must pull the photo from the VOXL 2 (that lives in /data/snapshots) and then can look at the properties of the image via a double click or by uploading the image to a website that allows for metadata from the EXIF to be viewed.