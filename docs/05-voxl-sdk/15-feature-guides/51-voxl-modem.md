---
title: VOXL Modems
layout: default
parent: Feature Guides
has_children: true
nav_order: 51
permalink: /voxl-modem/
---

# voxl-modem

<a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem/-/commits/master" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem/-/commits/master" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem/badges/master/pipeline.svg?key_text=Master+Pipeline&amp;key_width=100&amp;ignore_skipped=true"></a> <a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem/-/commits/dev" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem/-/commits/dev" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem/badges/dev/pipeline.svg?key_text=Dev+Pipeline&amp;key_width=85&amp;ignore_skipped=true"></a> <a class="gl-mr-3" data-qa-link-url="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem/-/releases" data-qa-selector="badge_image_link" href="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem/-/releases" rel="noopener noreferrer" target="_blank"><img alt="Project badge" aria-hidden="" class="project-badge" src="https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem/-/badges/release.svg?key_text=Latest Release&amp;key_width=100"></a>

## Overview

<i class="fab fa-gitlab" style="color: #e66100;"></i>
[voxl-modem](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem) 

The `voxl-modem` package provides software support for the various ModalAI supported modem hardware options, such as:

- LTE Modems (Sierra)
- 5G Modems (Quectel)
- Microhard Modems
- Doodle Labs Modems
- DTC Modems (Community Supported)

## Usage

As with most SDK services, `voxl-modem` can be configured and enabled to run on boot using it's configuration script. Usage is as follows:

```
voxl2:/$ voxl-configure-modem -h
Making new config file /etc/modalai/voxl-modem.conf

Config script for voxl-modem


voxl-configure-modem enable
voxl-configure-modem disable
voxl-configure-modem factory_enable
voxl-configure-modem doodle
voxl-configure-modem dtc

show this help message:
voxl-configure-modem help
```

Running `voxl-configure-modem` without providing an argument, will execute a wizard with the following options:

```
voxl2:/$ voxl-configure-modem 
Starting interactive mode
 
What type of modem are you using?

1) v2
2) microhard
3) dtc
4) doodle
5) quectel
6) em9191
#? 
```

Selecting an option from this menu will trigger more questions catered to that modem type:

```
voxl2:/$ voxl-configure-modem 
Starting interactive mode
 
What type of modem are you using?

1) v2
2) microhard
3) dtc
4) doodle
5) quectel
6) em9191
#? 5
 
Are you attempting to connect to ModalLink? (not common)
1) yes
2) no
#? 2
 
Which APN is correct for your SIM card?

AT&T - IoT device - APN: m2m.com.attz
AT&T - Laptop or Tablet - APN: broadband
AT&T - Smartphone - APN: phone
T-Mobile - APN: fast.t-mobile.com
Verizon - APN: vzwinternet
Google Fi - APN: h2g2

1) m2m.com.attz	      4) fast.t-mobile.com  7) Custom
2) broadband	      5) vzwinternet
3) phone	      6) googlefi
#?
```

Going through the `voxl-modem` configuration process will set up a configuration file located at `/etc/modalai/voxl-modem.conf` as well as enable the `voxl-modem` service to run on boot

```
voxl2:/$ cat /etc/modalai/voxl-modem.conf 
{
	"modem_type":	"quectel",
	"apn":	"h2g2",
    "world": "false",
	"microhard_ip": "192.168.168.100",
        "dtc_ip":       "192.168.0.100",
	"doodle_ip": "10.223.0.100",
	"modallink":	"false",
	"placeholder": "hello world"
}

```

```
voxl2:/$ voxl-configure-modem doodle 
wiping old config file
reloading systemd services
enabling voxl-modem systemd service
Created symlink /etc/systemd/system/multi-user.target.wants/voxl-modem.service → /etc/systemd/system/voxl-modem.service.
starting voxl-modem systemd service
DONE configuring voxl-modem
```

It is possible to modify this config file manually in some circumstances, but not recommended.

## Tips

When checking to see if a modem is enumerated correctly, it can be helpful to check `lsusb` to see if the device has been detected:

```
voxl2:/$ lsusb
Bus 002 Device 002: ID 0424:5744 Standard Microsystems Corp. 
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 004: ID 0424:2740 Standard Microsystems Corp. 
Bus 001 Device 003: ID 05ac:1402 Apple, Inc. Ethernet Adapter [A1277]
Bus 001 Device 002: ID 0424:2744 Standard Microsystems Corp. 
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

Another helpful command is `ifconfig`, which displays the VOXL's network interfaces and their IP addresses:

```
voxl2:/$ ifconfig
bond0: flags=5123<UP,BROADCAST,MASTER,MULTICAST>  mtu 1500
        ether 2a:f2:52:11:bf:f8  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

dummy0: flags=195<UP,BROADCAST,RUNNING,NOARP>  mtu 1500
        inet6 fe80::10:e3ba:b70f:f411  prefixlen 64  scopeid 0x20<link>
        ether 3e:a1:0a:d0:b3:40  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 8  bytes 1648 (1.6 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

eth0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        ether 48:d7:05:ea:06:36  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 200  bytes 14500 (14.5 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 200  bytes 14500 (14.5 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

`voxl-modem` can be disabled from running on boot via. the following:

```
voxl2:/$ voxl-configure-modem disable
disabling voxl-modem systemd service
stopping voxl-modem systemd service
Done configuring voxl-modem
```

As with most services, it is recommended to reboot your device after doing so.

To check the status of `voxl-modem` when it is running, use the following:

```
systemctl status voxl-modem
```