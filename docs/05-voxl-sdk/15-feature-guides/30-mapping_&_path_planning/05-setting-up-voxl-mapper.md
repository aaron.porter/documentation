---
layout: default
title: Setting up VOXL Mapper
parent: Mapping and Path Planning
search_exclude: true
nav_order: 05
permalink: /setting-up-voxl-mapper/
---

# Setting up VOXL Mapper
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Flying with VOXL Mapper

### Confirm VOXL Vision Hub is correctly setup
VOXL Mapper heavily relies on [voxl-vision-hub](/voxl-vision-hub/) and the VIO system. As such, it is important that you have correctly setup both of these before attempting to run voxl-mapper. Setup for voxl-vision-hub can be found here [Flying with VIO](/flying-with-vio/):

### Confirm Figure Eight Offboard Mode is working
Prior to running any of the planning capabilities it is important to ensure that the figure eight offboard mode is working correctly. A short guide on enabling figure eight mode can be found here: [Offboard Figure Eight Mode](/voxl-vision-hub/)

### Set Offboard Mode to trajectory
In order for voxl-mapper to be able to command the robot to move along the desired trajectory, the offboard mode must be changed. In the `/etc/modalai/voxl-vision-hub.conf` configuration file ensure that the offboard mode parameter is set to trajectory:

```json
"offboard_mode":    "trajectory",
```

### Take off in Position Mode
It is safer to take off in position flight mode than it is to flip to this mode mid-flight from manual flight mode. The quadcopter should take off straight up and be much easier to control when in position flight mode. Be ready to flip back to manual mode should anything go wrong. It is safer to flip to manual mode and land than to kill the motors mid-flight. Killing the motors mid-flight may result in the propellers loosening from the reverse-torque and flying straight up off the quadcopter. Only kill the motors after landing and spooling down the motors OR in a serious emergency.

### Change to Offboard Mode
While flying in position mode, make sure the quadcopter is hovering comfortably before changing the flight mode to offboard mode. The quadcopter should hold its position. In offboard mode the two joysticks on the RC controller are ignored and the PX4 follows any commands given to it by VOXL over the UART link. In this case VOXL will wait for a plan from voxl-mapper.
