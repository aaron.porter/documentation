---
title: Replaying
layout: default
parent: Feature Guides
has_children: true
nav_order: 60
permalink: /voxl-replay/
---


# voxl-replay

voxl-replay plays back MPA logs that were captured by [voxl-logger](/voxl-logger/)

Example of playing back a voxl-logger log stored in /data/voxl-logger/log0003:

```voxl-replay -p /data/voxl-logger/log0003```