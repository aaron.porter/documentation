---
title: Core SDK Libraries
layout: default
parent: VOXL SDK
has_children: true
nav_order: 45
permalink: /core-libs/
---

# Core SDK Libs

The pages in this section explain the inner workings of core libraries that the VOXL SDK is built on. This is a highly recommended read if you plan to build custom applications for VOXL that will depend on these.