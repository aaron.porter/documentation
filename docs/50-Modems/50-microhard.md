---
layout: default3
title: Microhard Modems
parent: Modems
nav_order: 40
has_children: true
permalink: /microhard-modems/
thumbnail: /modems/microhard/Microhard-pMDDL2350.png
buylink: https://www.modalai.com/collections/voxl-add-ons
summary: ModalAI's range of Microhard modems
---

# ModalAI Microhard Modems
{: .no_toc }

{:toc}
Documentation for ModalAI's range of Microhard modems.  

<a href="https://www.modalai.com/collections/voxl-add-ons" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/11/microhard-modems" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>

![microhard](/images/modems/microhard/Microhard-pMDDL2350.png)
