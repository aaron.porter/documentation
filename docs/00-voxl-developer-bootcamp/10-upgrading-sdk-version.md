---
layout: default
title: 10. Upgrading VOXL SDK
parent: VOXL Developer Bootcamp
nav_order: 10
permalink: /flash-system-image/
---

# Upgrading VOXL SDK
{: .no_toc }

---
## Do I Need to Upgrade VOXL SDK?

All VOXL's ship with a recent version of the SDK installed and all the core services enabled. Upgrading is optional but recommended as the SDK is constantly improving and adding new features.

To decide if you need to upgrade or not, compare the "voxl-suite" version you just retrieved in the [previous step](/voxl-version/) with the latest release version on the [VOXL SDK Release Notes page](/voxl-suite/).

Note: The VOXL-SDK installer used to be called a "Platform Release" but we now simply call it "The VOXL-SDK".



## Updating Patch Versions

Patch version bumps, e.g. V1.0.0 to V1.0.1 do not require a system image upgrade and can be upgrading in place. It is recommended to do this periodically to pull down the latest bug fixes.

To do this, ensure that your VOXL is connected to the internet and run these commands:

```
voxl2:/$ apt update && apt upgrade
```

This will upgrade VOXL SDK services and utilities without needing to wipe the VOXL by flashing a system image. If configuration files have been expanded to support new features, your existing configuration files will be expanded with the new defaults. For more, see the [VOXL SDK Configuration Section](/mpa-configuration-conventions/).

Note that any changes you've made to service states or config files will be preserved when using apt-upgrade. New ESC Firmware or ELRS firmware will also not be loaded during the upgrade until the user manually runs those configuration steps either manually or with the voxl-configure-mpa tool.

You can run `voxl-configure-mpa` as well. This will flash any new ESC firmware onto the ESC and reconfigure all services to their new defaults. For more information see the [voxl-configure-mpa page](/voxl-configure-mpa/).




## Updating Minor Versions

Minor version bumps, e.g. V1.0.0 to V1.1.0 require a new system image to be flashed. To do this, download and flash your VOXL with a complete VOXL-SDK installer.

Note, you can also elect to upgrade a patch version, e.g. from V1.0.0 to V1.0.1 with a VOXL-SDK installer to ensure you are returning your VOXL to a known default state.




## Flashing a VOXL-SDK Release

{: .alert .warning-alert}
**Note:** This process will preserve everything in VOXL's `/data/` directory (i.e. sku.txt, calibration files, image snapshots, and video recordings), but everything else (i.e. all services and config files) will be set to the factory defaults.

VOXL Suite, containing ModalAI's software tools/services, is the same for all VOXL hardware. However, different hardware calls for different System Images. As such, VOXL, VOXL 2, and VOXL 2 mini all have unique SDK's. (VOXL flight contains the same hardware as VOXL and shares SDK's).

**1) Where to Download**
SDK releases can be found on our <a href="https://developer.modalai.com/categories" target="_blank">Protected Downloads Page</a>

* <a href="https://developer.modalai.com/asset/1" target="_blank">VOXL & VOXL Flight (APQ8096) SDK's</a>
* <a href="https://developer.modalai.com/asset/2" target="_blank">VOXL 2 (QRB5165) SDK's</a>
* <a href="https://developer.modalai.com/asset/10" target="_blank">VOXL 2 Mini (QRB5165) SDK's</a>

<br />

**2) Power up VOXL and Connect via USB**

{: .alert .simple-alert}
**Note:** We've seen some computers where USB C to USB C cables are not working well. If you have having issues with connections, please try a USB C to USB A cable.

<br />

**3) Run Install Script**
**NOTE**: Do **NOT** open a bash shell in your VOXL. The follow commands are all run on the host computer, not on VOXL.

- Unzip the download, in this example we'll assume the download name was `voxl2_SDK_M.m.b.tar.gz` where M.m.b is the version of the SDK.

```bash
me@mylaptop:~/$ tar -xzvf voxl2_SDK_M.m.b.tar.gz
```

- Get ready to run the script by going into the directory you just unzipped

```bash
me@mylaptop:~/$ cd voxl2_SDK_M.m.b
```

- Run the following:

```bash
me@mylaptop:~/$ ./install.sh
```

{: .alert .warning-alert}
After installation begins (you confirm that you would "like to continue with the ... system image flash"), DO NOT STOP IT! Stopping the flashing process in the middle will likely corrupt the file system. If you do need to recover a corrupted file system, follow [this guide](/voxl2-unbricking/).

- Depending on SDK version, you may be prompted to preserve or wipe the /data/ partition. When in doubt, preserve (DO NOT WIPE) the data partition if you don't explicitly intend to. It contains calibration and factory configuration data that should be preserved between SDK upgrades.

- If everything flashed successfully, you should see a message during the install process:
```
=====================================================
|     Done installing voxl-suite for QRB5165        |
|                                                   |
|         Please visit our online guides at         |
|             https://docs.modalai.com/             |
|                                                   |
|      To configure Modal Pipe Architecture (MPA)   |
|       services, please run voxl-configure-mpa     |
|                                                   |
| To see what MPA services are enabled and running, |
|           please run voxl-inspect-services        |
|                                                   |
|   To see a list of MPA utilities available, adb   |
|      or ssh into VOXL and type voxl{TAB}{TAB}     |
=====================================================
```
Flashing  SDK 1.0 and above will also automatically reconfigure your drone's SKU and MPA (Modal Pipe Architecture) services.  If these prompts were part of the SDK-flashing process, then you have already configured SKU and MPA and do not need to re-run these commands:

<img src="/images/voxl-developer-bootcamp/voxl-configure-sku.png" alt="voxl-configure-sku prompt"/>

<img src="/images/voxl-developer-bootcamp/voxl-configure-mpa.png" alt="voxl-configure-sku prompt"/>


<br>
[Next: VOXL Portal](/voxl-portal/){: .btn .btn-green }
