---
layout: default
title: 2.4 Sentinel
parent: 2. Hardware Quickstart
nav_order: 25
has_children: false
permalink: /sentinel-hardware-quickstart/
---

# Sentinel Hardware Quickstart
{: .no_toc }

This section contains a hardware quickstart and is intended for developer type users.  It provides the minimum steps needed to get a device powered up and ready to connect to a console.

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

For technical details, see the [datasheet](/sentinel-datasheet/).

## Quickstart Video

{% include youtubePlayer.html id="hMhQgWPLGXo" %}

## How to Power On and Prepare to Connect

### 1. Ensure propellors are removed

<img src="/images/sentinel/sentinel-unplugged.jpg" alt="sentinel-unplugged" width="1280"/>


### 2. Connect USB-C Cable

- Connect USB-C cable to VOXL 2
- Connect other side of USB-C cable (recommmended USB type A) to host computer for later use

<img src="/images/sentinel/d0006-bootcamp-quickstarts.jpg" alt="d0006-bootcamp-quickstarts" width="1280"/>

### 3. Power ON

- Connect the battery or wall power supply to the XT60 connector (labeled `Power` above)
  - *NOTE: do not arm the vehicle while using wall power*

The development drone will bootup and make play a jingle via the ESC.

## How to Power Off

Pulling the power is okay in normal operation. If you are logged into VOXL and have been modifying files on the file system then it's recommended to run `sync` to make sure your changes are flushed to flash memory before pulling power.

Do not pull power while flashing software as this can brick the device.

[Next Step: Set up ADB](/setting-up-adb/){: .btn .btn-green }
