---
layout: default
title: 2. Datasheet
parent: Starling 2 Max
nav_order: 10
has_children: false
permalink: /starling-2-max-datasheet/
---

# Starling 2 Max Datasheet
{: .no_toc }

## Specifications

| Component       | Specification                                                                          |
|-----------------|----------------------------------------------------------------------------------------|
| Autopilot       | [VOXL2](/voxl2/)                                                                       |
| Image Sensors   | C28 Config - Dual IMX412, Dual AR0144 or C29 Config - Dual IMX412, Dual AR0144, TOF    |
| Take-off Weight | 566g                                                                                   |
| Payload Capacity| 500g - will affect flight time with total takeoff weight ~930g                         |
| Diagonal Size   |                                                                                        |
| Max Speed       | Cruise 15m/s, Dash (or burst) 20m/s                                                    |
| Wind Gusts      | Tolerant to 15m/s                                                                      |
| Service Ceiling | 4,000m MSL                                                                             |
| Flight Time     | ~55 minutes (with Amprius cells, ~40 min with LiIon)                                   |
| Motors          | 1504 3000kv                                                                            |
| Propellers      | 120mm                                                                                  |
| Frame           | 3mm Carbon Fiber                                                                       |
| ESC             | [ModalAI 4-in-1 Mini ESC](/voxl-mini-esc-datasheet/)                                   |
| GPS             | UBlox M10                                                                              |
| RC Receiver     | 915mhz ELRS or 2.4GHz Ghost Atto                                                       |
| Datalink        | WiFi: AlfaNetworks AWUS036EACS, FCC ID: 2AB878811                                      |
| Power Module    | Integrated with ModalAI 4-in-1 Mini ESC                                                |
| Battery         | Sony VTC6 3000mah 2S, or any 2S 18650 LiIon battery with XT30 connector                |
| Height          |                                                                                        |
| Width           |                                                                                        |
| Length          |                                                                                        |


## Hardware Wiring Diagram

[D0012-V1-compute-wiring](/voxl2-d0014/)

Diagram draw.io Source

## 3D STEP

* [Starling 2 Max 3D STEP R2](https://developer.modalai.com/asset/download/167) (October 2024 onwards)
* [Starling 2 Max 3D STEP R1](https://developer.modalai.com/asset/download/159) (April through September 2024)

## Add-On Board

The add-on board used on this vehicle has an I2C, UART and GPIOs along with the USB interface.  See [datasheet](/usb2-type-a-breakout-add-on/) for pinouts.
