---
layout: default3
title: Seeker
nav_order: 45
has_children: true
permalink: /seeker/
parent: Legacy EOL Dev Drones
summary: The world's first SLAM development drone with SWAP-optimized sensors and payloads optimized for indoor and outdoor navigation.
thumbnail:  /seeker/Seeker-whitebackg.png
buylink: https://www.modalai.com/collections/development-drones/products/seeker
---

# Seeker
The world's first micro-development drone with SWAP-optimized sensors and payloads optimized for indoor and outdoor navigation. 

{: .no_toc }
<a href="https://www.modalai.com/collections/development-drones/products/seeker" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/30/seeker" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![Seeker](/images/seeker/Seeker-whitebackg.jpg)
