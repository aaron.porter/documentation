---
layout: default
title: Seeker Power Supply and Battery
parent: Seeker User Guide
nav_order: 15
has_children: false
permalink: /seeker-user-guide-power/
---

# Seeker Power Supply and Battery
{: .no_toc }

This guide will walk you from taking the Seeker out of the box and up into the air!

For technical details, see the [datasheet](/seeker-datasheet/).

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


{: .alert .danger-alert}
**WARNING:** For setup and configuration of your Seeker you must <b> Remove the Propellers </b> for safety.

## Battery Installation

Seeker requires a 3 cell lithium ion battery with an XT30 connector. To install a 3S battery into seeker, open the battery cage, and place the 3S battery in. Make sure the battery's wires are facing downwards. Then, take the balance lead, and string it through the top of the locking mechanism as shown. Once the cabling is out of the way, push the latch down until it locks.

![seeker-battery-gif](/images/seeker/seeker-bat-gif.gif)

## Power On

Power on Seeker by connecting the battery's XT30 connector onto the other end of the connector on the bottom side of the drone as shown below.

{: .alert .danger-alert}
**WARNING:** Keep a fan on the back of Seeker's VOXL-CAM when the drone is powered on in a static state to avoid the unit from overheating.


![seeker-battery-gif-2](/images/seeker/seeker-connect-bat.gif)

## Benchtop Power Supply

For desktop use, a 12VDC wall power supply can be used and is [available here](https://www.modalai.com/products/ps-xt60).  this is convenient when doing development.

---

[Next Step: Connect to Seeker](/seeker-user-guide-adb/){: .btn .btn-green }
