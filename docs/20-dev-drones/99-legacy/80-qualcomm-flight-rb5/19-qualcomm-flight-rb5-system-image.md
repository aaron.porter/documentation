---
layout: default
title: Qualcomm Flight RB5 System Image
parent: Qualcomm Flight RB5 
nav_order: 19
has_children: false
permalink: /Qualcomm-Flight-RB5-system-image/
youtubeId: SGflQpCfsZs
search_exclude: true
---

# Qualcomm Flight RB5 System Image

[Buy Here](https://www.modalai.com/products/qualcomm-flight-rb5-5g-platform-reference-drone)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL SDK for RB5 Flight

**Update 2022-07-20**: the Qualcomm Flight RB5 vehicle is now supported by the VOXL SDK!  It requires RB5 Flight System Image 1.3 or newer.

A Platform Release that includes both an updated system image and the VOXL SDK is available at [downloads.modalai.com](https://downloads.modalai.com).

The following tutorial guides you through upgrading to VOXL SDK:

{% include youtubePlayer.html id=page.youtubeId %}

If using the legacy RB5 Flight SDK, continue below:

## What Is It?

The system image is essentially "the operating system", and consists of the kernel, root file system, and various firmwares for the plentiful amount of processors on board to name a few items.

## Do I need to Install?

If you have a `Qualcomm Flight RB5`, it comes pre-installed so there's nothing to do.

Occasionally we will ship updates, and you can follow the update procedure below.

If you are having problems with your system, **please** ask questions on the [forum](https://forum.modalai.com/category/22/qualcomm-flight-rb5-5g-drone) before reflashing the system image.

## Where To Download

Please visit <https://downloads.modalai.com> and download the newest `Qualcomm Flight RB5 System Image`.

## Backup Files

- Please backup the following manually for now:

```bash
adb pull /etc/modalai .
adb pull /data/misc .
```

{: .alert .danger-alert}
**WARNING:** ALL DATA IS LOST IN THE CURRENT SYSTEM IMAGE INSTALLATION

## How to Upgrade

- Unzip the download, in this example we'll assume the download name was `M.m.b-M0052-9.1-perf.tar.gz` where M.m.b is the version.

```bash
tar -xzvf M.m.b-M0052-9.1-perf.tar.gz
```

- Get ready to run the script by going into the directory you just unzipped

```bash
cd M.m.b-M0052-9.1-perf
```

- Now, attach the RB5 via USBC and ensure the unit is powered on
- Run the following:

```bash
sudo ./full-flash.sh
```

## Upgrade using Fastboot

### Overview

Fastboot is used to program the device. You may need to force the processor directly into fastboot if the unit is bricked and it no longer shows up as an ADB device. In the full-flash script, ADB is used to enter fastboot. If you cannot get to ADB, you need to force the device into fastboot by performing the following steps.

### Procedure

To access the fastboot switch on your RB5 Flight Deck, use the access hole shown on the left. The switch location is indicated on the PCB on the right. 

![rb5-fastboot-1](/images/rb5/fastboot.jpg)

- Unplug RB5 Flight from power
- Unplug RB5 Flight from USBC
- Using something soft like a BBQ skewer or toothpick, press and hold the momentary button `SW1` down, as shown in this image:

![rb5-fastboot-2](/images/rb5/rb5-debug-stick.gif)

- While holding `SW1` down, power on RB5 Flight
- Keep holding `SW1` down for about 5 seconds and then let go
- Attach RB5 Flight to USBC connected to host computer
- Run the `fastboot devices` command and verify the device is showing up

```bash
❯ fastboot devices
f8bb8d44	fastboot
```

- Now, you should be able to proceed with the `./full-flash.sh` command


## What's Next

You should [install the SDK](/Qualcomm-Flight-RB5-sdk-installation/).

## Release Notes

Coming soon.
