---
layout: default
title: RB5 SDK Services
parent: Qualcomm Flight RB5 SDK
nav_order: 6
has_children: false
permalink: /Qualcomm-Flight-RB5-sdk-services/
search_exclude: true
---

# Qualcomm Flight RB5 SDK
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


## Summary

On `Qualcomm Flight RB5`, the `systemd` suite is used to start services on bootup.  For example, we have a service called `rb5-chirp-server.service` that, in the factory state, configures the unit start up chirp sensors on bootup!

## How to Configure

The `Qualcomm Flight RB5` ships pre-configured.  If you need to reconfigure, see [here](/Qualcomm-Flight-RB5-sdk-installation/) for details.

To use the factory default, you can use the following:

```bash
./configure-sdk.sh factory_enable
```

## Bootup Sequence

Below describes the boot up sequence.

![rb5-services](/images/rb5/rb5-sdk/rb5-services.png)
