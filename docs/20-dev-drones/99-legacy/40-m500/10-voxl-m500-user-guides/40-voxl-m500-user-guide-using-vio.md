---
layout: default
title: VOXL m500 Using VIO
parent: M500 User Guides
nav_order: 30
has_children: false
permalink: /voxl-m500-user-guide-using-vio/
---
# m500 VIO
{: .no_toc }

1. TOC
{:toc}

## Enabling VIO for GPS-denied Navigation

VIO on m500 is disabled by default. The service file for VIO can be started by configuring the qvio server:
```bash
voxl-qvio-server-configure
```
It will show the following prompt:
```bash
Would you like to enable the qvio service?
This will start qvio as a background process
Selecting yes will start the service, no will disable the service

1) yes
2) no
```

Enter ```1 (yes)``` to enable the service. This will immediately start VIO and enable it on boot.

## Using VIO
In order to check if VIO is running you can check with the following command:
```bash
systemctl status voxl-qvio-server
```

This will show you the status of the voxl-qvio-server. 

If the status displays ```active(runnning)``` then VIO is running and the VIO data is being sent to PX4.

If the status displays ```failed``` then VIO failed to run. Make sure that camera index 2(downward facing tracking camera) and PX4 are running.

To restart VIO, you can simply restart the service file:
```bash
systemctl restart voxl-qvio-server
```

VIO should be restarted and the drone should be idle before any flight where VIO will be used.

## Viewing VIO Data
VIO data can be viewed by manually running the voxl-inspect-qvio command:

```bash
voxl-inspect-qvio
```

**NOTE** A library path was set for this command, if you're command doesn't execute, inside your adb shell run ```ldconfig```

This will print out position data in the following format:
```
    T_imu_wrt_vio (m)   |Roll Pitch Yaw (deg)| state| error_code
   -0.11   -0.12   -0.06| -14.6  -30.1   -2.6| OKAY | 
```

T_imu_wrt_vio represents your drones XYZ position in meters from your starting position. To validate this data, you can move the drone forwards and observe as the X value increases.

## Flying with VIO

### Confirm PX4 is Getting VIO Data

Once voxl-qvio-server is running, confirm quality data before flight by opening the mavlink inspector and checking that X&Y values are non-zero in the ODOMETRY mavlink telemetry packets.

![voxl-vio-mavlink-inspector.png](/images/voxl-sdk/voxl-vio-mavlink-inspector.png)

Alternatively, if your computer running QGroundControl has its volume turned up you should hear it announce the swap from "manual" to "position" mode when you flip the flight mode switch from up to middle. If VIO data is not being transmitted, the system will fall back to altitude mode and you will hear "position mode rejected, altitude flight mode". In this case, go back to manual flight mode (switch up) and wait a few more seconds for voxl-qvio-server to finish initializating.

If this doesn't work, go back to "Viewing VIO Data" and reconfirm VIO is working with voxl-inspect-qvio. You can also check the voxl-qvio-server systemd service for indications of what might have gone wrong. We have a VIO parameters file helper [located here](https://gitlab.com/voxl-public/flight-core-px4/px4-parameters/-/tree/master/helpers/rb5) to help easily configure into VIO or GPS mode. Specifically the **_EKF2_AID_MASK_** is often overlooked as an important parameter for VIO flight.

Troubleshooting VIO Guide is [here](https://docs.modalai.com/flying-with-vio/#troubleshooting-vio)

Some relevant PX4 parameters for VIO flight

```
# enable vision for height, position, and velocity, no GPS or barometer
1	1	EKF2_AID_MASK	280	6
1	1	EKF2_HGT_MODE	3	6
## set MD to 0 to allow covarience from VIO to be used
1	1	EKF2_EV_NOISE_MD	0	6
## Turn off EV Delay, VOXL will do proper timesyncing
1	1	EKF2_EV_DELAY	5.00	9
## increase bias limit to prevent preflight check failure
1	1	COM_ARM_EKF_AB 0.01	9
## Allow arming without GPS indoors
1	1	COM_ARM_WO_GPS	1	9
```

### Confirm Data is in the Right Coordinate Frame

While looking at the ODOMETRY data in QGroundControl's Mavlink Inspector, observe the xyz data as you move the quadcopter around. Make sure it aligns with NED coordinate frame. If there is a coordinate system mismatch PX4 will initially take off then very quickly run away out of control.

### Take off in Position Mode

It is safer to take off in Position Flight Mode than it is to flip to this mode mid-flight from Manual Flight Mode.

Take off and land with the same instructions as listed above but this time with the Flight Mode switch in the middle position. The quadcopter should take off straight up and be much easier to control than in manual mode.

Be ready to flip back to manual mode should anything go wrong. It is safer to flip to manual mode and land than to kill the motors mid-flight. Killing the motors mid-flight may result in the propellers loosening from the reverse-torque and flying straight up off the quadcopter. Only kill the motors after landing and spooling down the motors OR in a serious emergency.


[Next Step: Gimbal User Guide](/voxl-m500-gimbal-user-guide/){: .btn .btn-green }