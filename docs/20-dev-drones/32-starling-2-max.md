---
layout: default
title: Starling 2 Max
nav_order: 32
has_children: true
permalink: /starling-2-max/
parent: Dev Drones
summary: Starling 2 Max is our flagship development drone for both indoor and outdoor flight.
thumbnail: /starling-v2/starling-v2-hero-1.png
buylink: https://www.modalai.com/starling-2
---

# Starling 2 Max
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

Starling 2 Max is our flagship development drone for both indoor and outdoor flight.

