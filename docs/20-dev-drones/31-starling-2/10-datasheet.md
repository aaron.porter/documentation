---
layout: default
title: 2. Datasheet
parent: Starling 2
nav_order: 10
has_children: false
permalink: /starling-2-datasheet/
---

# Starling Datasheet
{: .no_toc }

## Specifications

| Component       | Specification                                                                          |
|-----------------|----------------------------------------------------------------------------------------|
| Autopilot       | [VOXL2](/voxl2/)                                                                       |
| Image Sensors   | C26 Config - IMX412, TOF, Dual AR0144<br>C26 Config - IMX412, TOF, Triple AR0144       | 
| Take-off Weight | 285g (182g without battery)                                                            |
| Diagonal Size   | 230mm                                                                                  |
| Flight Time     | >35 minutes                                                                            |
| Motors          | 1504 3000kv                                                                            |
| Propellers      | 120mm                                                                                  |
| Frame           | 3mm Carbon Fiber                                                                       |
| ESC             | [ModalAI 4-in-1 Mini ESC](/voxl-mini-esc-datasheet/)                                   |
| GPS             | UBlox M10                                                                              |
| RC Receiver     | 915mhz ELRS or 2.4GHz Ghost Atto                                                       |
| Datalink        | WiFi: AlfaNetworks AWUS036EACS, FCC ID: 2AB878811                                      |
| Power Module    | Integrated with ModalAI 4-in-1 Mini ESC                                                |
| Battery         | Sony VTC6 3000mah 2S, or any 2S 18650 battery with XT30 connector                      |
| Height          |                                                                                        |
| Width           |                                                                                        |
| Length          |                                                                                        |


## Hardware Wiring Diagram

D0014-V1-compute-wiring

Diagram draw.io Source

## 3D STEP

* [Starling 2 C26 3D STEP](https://developer.modalai.com/asset/download/158)
* [Starling 2 C27 3D STEP](https://developer.modalai.com/asset/download/157)

## Add-On Board

The add-on board used on this vehicle has an I2C, UART and GPIOs along with the USB interface.  See [datasheet](/usb2-type-a-breakout-add-on/) for pinouts.
