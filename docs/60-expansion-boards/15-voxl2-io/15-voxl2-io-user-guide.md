---
layout: default
title: VOXL 2 IO User Guide
parent: VOXL 2 IO
nav_order: 15
has_children: false
permalink: /voxl2-io-user-guide/
---

# VOXL 2 IO User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

<img src="/images/voxl2-io/user-guide/M0065-hero.png" width="240"/>

## Summary

### VOXL SDK Support

- See the [voxl2-io firmware changelog](/voxl2-io-firmware/#release-notes) for details
- Note: this documentation requires VOXL SDK 1.1.1 or newer (recommend SDK 1.3.0+).  For documentation on the legacy PX4IO/VOXL SDK 1.1.0 and older usage, see [here](/voxl2-io-legacy-guide/)

### Important Update

Starting in VOXL SDK 1.1.1, the VOXL 2 IO system is being updated, with changes including:

- a new bootloader based on same bootloader as VOXL ESC
- a new firmware based on same firmware as VOXL ESC
- a new `voxl-px4` driver `voxl2-io` to support new firmware

Existing VOXL 2 IO may be updated but requires a bootloader update over the debug header.  This can be done in the field, or contact ModalAI to arrange a swap out if needed.

For updating the legacy firmware, please see the [firmware guide](/voxl2-io-firmware/).

## System Overview

In general, VOXL 2 IO provides PWM outputs along with additional RC inputs for the VOXL 2 / VOXL 2 Mini ecosystem.

### Block Diagram

<img src="/images/voxl2-io/user-guide/M0065-system.jpg" width="640"/>

### Components

The following components describe the VOXL 2 IO system:

- VOXL 2 IO Hardware (M0065)
  - voxl2-io firmware - application running on hardware that communicates with voxl-px4
  - voxl2-io bootloader - used for firmware updates over UART

- Hardware connection for power and 2-wire UART

- VOXL 2 (M0054) or VOXL 2 Mini (M0104)
  - VOXL SDK 1.1.1 or newer
    - voxl-px4 package
      - voxl2-io PX4 driver that communicates with VOXL 2 IO application
  - Command line tools for VOXL 2 IO (e.g. FW update)

## Wiring Guides

**NOTE** Connector colors may vary due to supply chain variations.

### Host Connection

#### VOXL 2 Host (M0054)

Using the default DSP UART QUP2 on J18:

<img src="/images/voxl2-io/user-guide/M0054-J18-M0065.jpg" width="640"/>

|           |           |
|-----------|-----------|
| Connectors | VOXL 2 `J18` to VOXL 2 IO `J4` |
| Cable     | [MCBL-00015](/cable-datasheets/#mcbl-00015) - 4pin-JST-GH-to-4pin-JST-GH cable, [Buy](https://www.modalai.com/products/mcbl-00015) |
| PX4 Config (`/etc/modalai/voxl-px4.conf`) | `ESC`=`VOXL2_IO_PWM_ESC`         |
| PX4 Config (SBUS RC, Ghost) | `RC`=`M0065_SBUS`         |
| PX4 Config (EXTERNAL/NO RC) | `RC`=`{SPEKTRUM, CRSF_MAV, CRSF_RAW, EXTERNAL, or FAKE_RC_INPUT}`         |

Note: the UART on J19 pins 10/11 may be used (QUP7) instead if required.  Note PX4 configuration is required.


#### VOXL 2 Mini Host (M0104)

Using the default DSP UART QUP7 on J19 pins 7/8:

<img src="/images/voxl2-io/user-guide/M0104-J19-M0065.jpg" width="480"/>

|            |           |
|----------- |-----------|
| Connectors | VOXL 2 `J19` to VOXL 2 IO `J4` |
| Cable      | TODO |
| PX4 Config (`/etc/modalai/voxl-px4.conf`) | `ESC`=`VOXL2_IO_PWM_ESC`         |
| PX4 Config (SBUS RC, Ghost) | `RC`=`M0065_SBUS`         |
| PX4 Config (EXTERNAL/NO RC) | `RC`=`{SPEKTRUM, CRSF_MAV, CRSF_RAW, EXTERNAL, or FAKE_RC_INPUT}`         |

Note: the UART on J19 pins 10/11 may be used (QUP7) instead if required.  Note PX4 configuration is required.

### RC Connections

#### SBUS RC

##### FrSky R-XSR

<img src="/images/voxl2-io/user-guide/M0065-rc-sbus-r-xsr.jpg" width="480"/>

|            |           |
|----------- |-----------|
| Connectors | VOXL 2 IO `J3` to R-XSR |
| Cable      | [MCBL-00065](/cable-datasheets/#mcbl-00065) |

##### Graupner GR-16

<img src="/images/voxl2-io/user-guide/M0065-rc-sbus-gr-16.jpg" width="480"/>

|            |           |
|----------- |-----------|
| Connectors | VOXL 2 IO `J3` to GR-16 |
| Cable      | [MCBL-00064](/cable-datasheets/#mcbl-00064) |

### PWM Out Connection

<img src="/images/voxl2-io/user-guide/M0065-pwm.jpg" width="480"/>

|            |           |
|----------- |-----------|
| Connectors | VOXL 2 IO `J1` to [M0022](https://modalai.com/collections/accessories/products/flight-core-pwm-output-cable-and-break-out-board) PWM Breakout Board |
| Cable      | [MCBL-00004](/cable-datasheets/#mcbl-00004) |

## LED Patterns

<img src="/images/voxl2-io/user-guide/M0065-leds.png"/>

### SDK 1.3.0+

**BLUE**

| State | Description |
|-------|-------------|
| 10Hz  | Bootloader mode, ~1 second duration after power cycle |
| 1 Hz  | Two (2) 500ms blinks to indicate valid FW |
| Off   | Idle, no PX4 activity/packets |
| 5Hz   | PX4 activity/packets |

**Orange**

| State | Description |
|-------|-------------|
| Off   | Idle, no RC packets |
| Blinking | Receiving RC packets |

**Green**

| State | Description |
|-------|-------------|
| Off   | No power |
| On    | Power |

## VOXL PX4 Configuration

See the [voxl2-io firmware changelog](/voxl2-io-firmware/#release-notes) for details on changes.

### SDK 1.3.0+

#### Actuator Setup using QGC

NOTE: 'Rev Range (for servos)` is not supported

The **Actuators** tab in QGroundControl will display a **VOXL 2 IO** tab that can be used to configure and test the outputs.

<img src="/images/voxl2-io/user-guide/M0065-qgc-actuator-v2.jpg">

#### PX4 Parameters

| Name                 | Description                              | Default | Units     |
|----------------------|------------------------------------------|---------|-----------|
| `VOXL2_IO_BAUD`      | Baud Rate <br> **Reboot Required**       | 921600  | bits/sec  |
| `VOXL2_IO_CMAX`      | Max PWM value used during calibration    | 2000    | us        |
| `VOXL2_IO_CMIN`      | Min PWM value used during calibration    | 1050    | us        |
| `VOXL2_IO_DIS `      | When disarmed, use this PWM value        | 1000    | us        |
| `VOXL2_IO_FUNC1`     | PWM CH1 Function                         | 101     | enum      |
| `VOXL2_IO_FUNC2`     | PWM CH2 Function                         | 102     | enum      |
| `VOXL2_IO_FUNC3`     | PWM CH3 Function                         | 103     | enum      |
| `VOXL2_IO_FUNC4`     | PWM CH4 Function                         | 104     | enum      |
| `VOXL2_IO_FUNC5`     | PWM CH5 Function                         | 0       | enum      |
| `VOXL2_IO_FUNC6`     | PWM CH6 Function                         | 0       | enum      |
| `VOXL2_IO_FUNC7`     | PWM CH7 Function                         | 0       | enum      |
| `VOXL2_IO_FUNC8`     | PWM CH8 Function                         | 0       | enum      |
| `VOXL2_IO_MAX`       | Max PWM value used when armed            | 2000    | us        |
| `VOXL2_IO_MIN`       | Min PWM value used when armed            | 1100    | us        |

#### How to Perform ESC Calibration

**REMOVE ALL PROPS, MOTORS MAY SPIN DURING CALIBRATION**

- power on VOXL2 and ESC while the PWM cable is not plugged into VOXL2. Calibration procedure only works before the ESC receives any valid signals
- start px4 and QGC and verify that the calibration parameters are set to suggested values (min 1050 and max 2000) and regular min and max values are set to min=1100 and max=2000.
- verify that the voxl2_io driver has started correctly : either qshell voxl2_io status  or look at the blue led on M0065 should be blinking which means it is receiving the pwm commands from voxl2_io driver (the disabled commands, since not armed)
- start the custom esc calibration procedure : “qshell voxl2_io calibrate_escs” and follow instructions (which are mainly just plug in the motor pwm cable into m0065 within 10 seconds)
- after the calibration is complete, test to make sure the PWM ranges have been correctly set:
  - using QGC actuator test, set the actuator from disabled to 1100 and the motors should spinup and stay on (no jerking)
  - it is encouraged to double check the motor starting point but temporarily lowering the VOXL2_IO_MIN param to 1000 and using actuator test to find the motor starting point, should be around 1060 (the calibration value was 1050). If the motor starting point is very close to 1100, it may not be safe to use this configuration, because if mixer commands 1100 during flight, variations due to temperature of the ESC MCU could cause 1100 command to be interpreted as DISABLED.
  - IMPORTANT: if VOXL2_IO_MIN was temporarily modified to find the motor starting point, revert it back to the desired value of 1100

### SDK 1.1.1+

#### Actuator Setup using QGC

NOTE: 'Rev Range (for servos)` is not supported

The **Actuators** tab in QGroundControl will display a **VOXL 2 IO** tab that can be used to configure and test the outputs.

<img src="/images/voxl2-io/user-guide/M0065-qgc-actuator.png">

#### PX4 Parameters

| Name               | Description                                                                                                  | Default      | Units    |
|:--------------------:|:--------------------------------------------------------------------------------------------------------------:|:----------------:|:-----------:|:-------:|
| `VOXL2_IO_BAUD`  | Baud Rate <br> **Reboot Required**       | 921600  |   bits/sec       |
| `VOXL2_IO_FUNC1`  | PWM 1 Function  | 101 |          |
| `VOXL2_IO_FUNC2`  | PWM 2 Function  | 102 |          |
| `VOXL2_IO_FUNC3`  | PWM 3 Function  | 103 |          |
| `VOXL2_IO_FUNC4`  | PWM 4 Function  | 104 |          |
| `VOXL2_IO_PWM_MIN`  | PWM Min       | 1000 |   microseconds       |
| `VOXL2_IO_PWM_MAX`  | PWM Max       | 2000 |   microseconds       |

#### How to Perform ESC Calibration

**REMOVE ALL PROPS, MOTORS MAY SPIN DURING CALIBRATION**
{% include youtubePlayer.html id="409LOMBYfb4" %}

**REMOVE ALL PROPS, MOTORS MAY SPIN DURING CALIBRATION**    
1. Power off drone
2. Disconnect the PWM output cable connected to J1 on VOXL 2 IO (MCBL-00004) 
3. Power on drone
4. Adb shell or ssh onto drone
5. Run to following command on your drone: ```px4-qshell voxl2_io calibrate_escs```
6. You now have 10 seconds to reconnect the cable for J1 on VOXL 2 IO (MCBL-00004) before the calibration process begins 
7. The calibration will write the PWM max value you have set by `VOXL2_IO_PWM_MAX` for 3 seconds
8. The calibration will write the PWM min value you have set by `VOXL2_IO_PWM_MIN` for 4 seconds
9. At the end of the calibration process, the ESCs/Motors will play a sequence of tones 
    
## Supported Use Cases

VOXL SDK 1.1.1 or newer required for the following use cases.

### VOXL 2 standard RC input and PWM Output

#### VOXL 2 (M0054)

<img src="/images/voxl2-io/user-guide/M0065-use-cases-M0054-M0065-ELRS-PWM.jpg" width="640"/>

| VOXL 2 Connector | Usage       | PX4 Driver |
|:------------------:|:-------------:|:------------:|
| J18              | PWM Out     | `voxl2_io` |
| J19              | RC, ELRS    | `crsf_rc`  |

| `/etc/modalai/voxl-px4.conf` | Value       | PX4 Driver Used |
|:------------------:|:-------------:|:---:|
| `ESC`              | `VOXL2_IO_PWM_ESC`     | `voxl2_io` |
| `RC`              |  `CRSF_RAW`    | `crsf_rc` |

#### VOXL 2 Mini (M0104)

<img src="/images/voxl2-io/user-guide/M0065-use-cases-M0104-M0065-ELRS-PWM.jpg" width="640"/>

| VOXL 2 Mini Connector | Usage       | PX4 Driver |
|:------------------:|:-------------:|:------------:|
| J19              | PWM Out     | `voxl2_io` |
| J19              | RC, ELRS    | `crsf_rc`  |

| `/etc/modalai/voxl-px4.conf` | Value       | PX4 Driver Used |
|:------------------:|:-------------:|:---:|
| `ESC`              | `VOXL2_IO_PWM_ESC`     | `voxl2_io` |
| `RC`              | `CRSF_RAW`    | `crsf_rc` |

### VOXL 2 IO S.BUS input and PWM Output

#### VOXL 2 (M0054)

<img src="/images/voxl2-io/user-guide/M0065-use-cases-M0054-M0065-SBUS-PWM.jpg" width="640"/>

| VOXL 2 Connector | Usage               | PX4 Driver |
|:------------------:|:---------------------:|:------------:|
| J18              | PWM Out, SBUS RC in | `voxl2_io` |

| `/etc/modalai/voxl-px4.conf` | Value       | PX4 Driver Used |
|:------------------:|:-------------:|:---:|
| `ESC`              | `VOXL2_IO_PWM_ESC`     | `voxl2_io` |
| `RC`              | `M0065_SBUS`    | `voxl2_io` |


#### VOXL 2 Mini (M0104)

<img src="/images/voxl2-io/user-guide/M0065-use-cases-M0104-M0065-SBUS-PWM.jpg" width="640"/>

| VOXL 2 Connector | Usage               | PX4 Driver |
|:------------------:|:---------------------:|:------------:|
| J19              | PWM Out, SBUS RC in | `voxl2_io` |

| `/etc/modalai/voxl-px4.conf` | Value       | PX4 Driver Used |
|:------------------:|:-------------:|:---:|
| `ESC`              | `VOXL2_IO_PWM_ESC`     | `voxl2_io` |
| `RC`              | `M0065_SBUS`    | `voxl2_io` |

### VOXL 2 IO S.BUS input and 4-in-1 UART ESC Output

#### VOXL 2 (M0054)

<img src="/images/voxl2-io/user-guide/M0065-use-cases-M0054-M0065-M0134.jpg" width="640"/>

| VOXL 2 Connector | Usage               | PX4 Driver |
|:------------------:|:---------------------:|:------------:|
| J18              | 4-in-1 ESC          | `voxl_esc` |
| J19              | SBUS RC in          | `voxl2_io` |

| `/etc/modalai/voxl-px4.conf` | Value       | PX4 Driver Used |
|:------------------:|:-------------:|:---:|
| `ESC`              | `VOXL_ESC`     | `voxl_esc` |
| `RC`              | `M0065_SBUS`    | `voxl2_io` |

## Common Issues
If none of the tips here help solve your issue, feel free to post a question on [https://forum.modalai.com/](https://forum.modalai.com/).

### One of my motors isn't spinning or my motors aren't all spinning the same rate
When this occurs, its usually because the ESCs need to be calibrated. If a ModalAI development drone is being used then the calibration procedure on this page can be used to calibrate the ESCs and this should help. If other ESCs are being used, you may want to refer to the manufacturers calibration procedure for your ESCs as there is no guarantee the calibrate procedure on this page will work.

### None of my motors are spinning
If none of your motors are spinning then there could be a variety of issues depending on your setup, it is a good idea to validate any of the following that are relevant:
- Check that all the cables in your setup connected to Voxl2 IO are secure
- Check that the voxl2_io driver is running in voxl-px4
- Check your RC connection if applicable 
- Check that all the correct parameters loaded, mainly the voxl2_io_enable_pwm.params from parameters for PX4 v1.14. 
- In QGC you can check the actuators tab as shown on this page to make sure that the motors are mapped correctly. You can also test the ESCs and motors by using the "Actuators Testing" section of the tab or via command line. 
